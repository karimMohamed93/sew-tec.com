<?php
/**
 * Created by Sublime Text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:20 AM
 */
 
/******************************************Core Function******************************************/
//Get option
if(!function_exists('s7upf_get_option')){
	function s7upf_get_option($key,$default=NULL)
    {
        if(function_exists('ot_get_option'))
        {
            return ot_get_option($key,$default);
        }

        return $default;
    }
}

//Get list header page
if(!function_exists('s7upf_list_header_page'))
{
    function s7upf_list_header_page()
    {
        global $post;
        $page_list = array();
        $page_list[] = array(
            'value' => '',
            'label' => esc_html__('-- Choose One --','lucky')
        );
        $args= array(
        'post_type' => 'page',
        'posts_per_page' => -1, 
        );
        $query = new WP_Query($args);
        if($query->have_posts()): while ($query->have_posts()):$query->the_post();
            if (strpos($post->post_content, '[s7upf_logo') ||  strpos($post->post_content, '[s7upf_menu')) {
                $page_list[] = array(
                    'value' => $post->ID,
                    'label' => $post->post_title
                );
            }
            endwhile;
        endif;
        wp_reset_postdata();
        return $page_list;
    }
}

//Get list sidebar
if(!function_exists('s7upf_get_sidebar_ids'))
{
    function s7upf_get_sidebar_ids($for_optiontree=false)
    {
        global $wp_registered_sidebars;
        $r=array();
        $r[]=esc_html__('--Select--','lucky');
        if(!empty($wp_registered_sidebars)){
            foreach($wp_registered_sidebars as $key=>$value)
            {

                if($for_optiontree){
                    $r[]=array(
                        'value'=>$value['id'],
                        'label'=>$value['name']
                    );
                }else{
                    $r[$value['id']]=$value['name'];
                }
            }
        }
        return $r;
    }
}

//Get order list
if(!function_exists('s7upf_get_order_list'))
{
    function s7upf_get_order_list($current=false,$extra=array(),$return='array')
    {
        $default = array(
            esc_html__('None','lucky')               => 'none',
            esc_html__('Post ID','lucky')            => 'ID',
            esc_html__('Author','lucky')             => 'author',
            esc_html__('Post Title','lucky')         => 'title',
            esc_html__('Post Name','lucky')          => 'name',
            esc_html__('Post Date','lucky')          => 'date',
            esc_html__('Last Modified Date','lucky') => 'modified',
            esc_html__('Post Parent','lucky')        => 'parent',
            esc_html__('Random','lucky')             => 'rand',
            esc_html__('Comment Count','lucky')      => 'comment_count',
            esc_html__('View Post','lucky')          => 'post_views',
            esc_html__('Like Post','lucky')          => '_post_like_count',
            esc_html__('Custom Modified Date','lucky')=> 'time_update',            
        );

        if(!empty($extra) and is_array($extra))
        {
            $default=array_merge($default,$extra);
        }

        if($return=="array")
        {
            return $default;
        }elseif($return=='option')
        {
            $html='';
            if(!empty($default)){
                foreach($default as $key=>$value){
                    $selected=selected($value,$current,false);
                    $html.="<option {$selected} value='{$value}'>{$key}</option>";
                }
            }
            return $html;
        }
    }
}

// Get sidebar
if(!function_exists('s7upf_get_sidebar'))
{
    function s7upf_get_sidebar()
    {
        $default=array(
            'position'=>'right',
            'id'      =>'blog-sidebar'
        );

        return apply_filters('s7upf_get_sidebar',$default);
    }
}

//Favicon
if(!function_exists('s7upf_load_favicon') )
{
    function s7upf_load_favicon()
    {
        $value = s7upf_get_option('favicon');
        $favicon = (isset($value) && !empty($value))?$value:false;
        if($favicon)
            echo '<link rel="Shortcut Icon" href="' . esc_url( $favicon ) . '" type="image/x-icon" />' . "\n";
    }
}
if(!function_exists( 'wp_site_icon' ) ){
    add_action( 'wp_head','s7upf_load_favicon');
    add_action('login_head', 's7upf_load_favicon');
    add_action('admin_head', 's7upf_load_favicon');
}

//Fill css background
if(!function_exists('s7upf_fill_css_background'))
{
    function s7upf_fill_css_background($data)
    {
        $string = '';
        if(!empty($data['background-color'])) $string .= 'background-color:'.$data['background-color'].';'."\n";
        if(!empty($data['background-repeat'])) $string .= 'background-repeat:'.$data['background-repeat'].';'."\n";
        if(!empty($data['background-attachment'])) $string .= 'background-attachment:'.$data['background-attachment'].';'."\n";
        if(!empty($data['background-position'])) $string .= 'background-position:'.$data['background-position'].';'."\n";
        if(!empty($data['background-size'])) $string .= 'background-size:'.$data['background-size'].';'."\n";
        if(!empty($data['background-image'])) $string .= 'background-image:url("'.$data['background-image'].'");'."\n";
        if(!empty($string)) return S7upf_Assets::build_css($string);
        else return false;
    }
}

// Get list menu
if(!function_exists('s7upf_list_menu_name'))
{
    function s7upf_list_menu_name()
    {
        $menu_nav = wp_get_nav_menus();
        $menu_list = array('Default' => '');
        if(is_array($menu_nav) && !empty($menu_nav))
        {
            foreach($menu_nav as $item)
            { 
                if(is_object($item))
                {
                    $menu_list[$item->name] = $item->slug;
                }
            }
        }
        return $menu_list;
    }
}

//Display BreadCrumb
if(!function_exists('s7upf_display_breadcrumb'))
{
    function s7upf_display_breadcrumb()
    {
        $breadcrumb = s7upf_get_value_by_id('s7upf_show_breadrumb','on');
        $breadcrumb_style = s7upf_get_value_by_id('s7upf_breadrumb_style');
        //if(empty($breadcrumb)) $breadcrumb = 'on';
        if($breadcrumb == 'on'){ 
            $b_class = s7upf_fill_css_background(s7upf_get_option('s7upf_bg_breadcrumb'));
            ?>
            <!-- <div class="row"> -->
                <div class="bread-crumb <?php echo esc_attr($b_class)?> <?php echo esc_attr($breadcrumb_style)?>">
                    <?php 
                        if(function_exists('bcn_display')) bcn_display();
                        else s7upf_breadcrumb();
                    ?>
                </div>
            <!-- </div> -->
        <?php }
    }
}

//Custom BreadCrumb
if(!function_exists('s7upf_breadcrumb'))
{
    function s7upf_breadcrumb() {
        global $post;
        $year = 'Y';
        if (!is_home() || (is_home() && !is_front_page())) {
            echo '<a href="';
            echo esc_url(home_url('/'));
            echo '">';
            echo esc_html__('Home','lucky');
            echo '</a>'.' <i class="fa fa-caret-right" aria-hidden="true"></i> ';
            if(is_home() && !is_front_page()){
                echo '<span>'.esc_html__('Blog','lucky').'</span>'; 
            }
            if (is_category() || is_single()) {
                the_category(' <i class="fa fa-caret-right" aria-hidden="true"></i> ');
                if (is_single()) {
                    echo ' <i class="fa fa-caret-right" aria-hidden="true"></i> ';
                    the_title();
                    echo '</span>';
                }
            } elseif (is_page()) {
                if($post->post_parent){
                    $anc = get_post_ancestors( get_the_ID() );
                    $title = get_the_title();
                    foreach ( $anc as $ancestor ) {
                        $output = '<a href="'.get_permalink($ancestor).'" title="'.esc_attr(get_the_title($ancestor)).'">'.get_the_title($ancestor).'</a> <i class="fa fa-caret-right" aria-hidden="true"></i><span> ';
                    }
                    echo balanceTags($output);
                    echo '<span> '.$title.'</span>';
                } else {
                    echo '<span> '.get_the_title().'</span>';
                }
            }
        }
        elseif (is_tag()) {single_tag_title();}
        elseif (is_day()) {echo"<span>".esc_html__("Archive for ","lucky"); the_time(get_option( 'date_format' )); echo'</span>';}
        elseif (is_month()) {echo"<span>".esc_html__("Archive for ","lucky"); the_time('F, Y'); echo'</span>';}
        elseif (is_year()) {echo"<span>".esc_html__("Archive for ","lucky"); the_time($year); echo'</span>';}
        elseif (is_author()) {echo"<span>".esc_html__("Author Archive ","lucky"); echo'</span>';}
        elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<span>".esc_html__("Blog Archives","lucky"); echo'</span>';}
        elseif (is_search()) {echo"<span>".esc_html__("Search Results","lucky"); echo'</span>';}
    }
}

//Get page value by ID
if(!function_exists('s7upf_get_value_by_id'))
{   
    function s7upf_get_value_by_id($key)
    {
        if(!empty($key)){
            $id = get_the_ID();
            if(is_front_page()) $id = (int)get_option( 'page_on_front' );
            if(!is_front_page() && is_home()) $id = (int)get_option( 'page_for_posts' );
            if(is_archive() || is_search()) $id = 0;
            if (class_exists('woocommerce')) {
                if(is_shop()) $id = (int)get_option('woocommerce_shop_page_id');
                if(is_cart()) $id = (int)get_option('woocommerce_cart_page_id');
                if(is_checkout()) $id = (int)get_option('woocommerce_checkout_page_id');
                if(is_account_page()) $id = (int)get_option('woocommerce_myaccount_page_id');
            }
            $value = get_post_meta($id,$key,true);
            if(empty($value)) $value = s7upf_get_option($key);
            return $value;
        }
        else return 'Missing a variable of this funtion';
    }
}

//Check woocommerce page
if (!function_exists('s7upf_is_woocommerce_page')) {
    function s7upf_is_woocommerce_page() {
        if(  function_exists ( "is_woocommerce" ) && is_woocommerce()){
                return true;
        }
        $woocommerce_keys   =   array ( "woocommerce_shop_page_id" ,
                                        "woocommerce_terms_page_id" ,
                                        "woocommerce_cart_page_id" ,
                                        "woocommerce_checkout_page_id" ,
                                        "woocommerce_pay_page_id" ,
                                        "woocommerce_thanks_page_id" ,
                                        "woocommerce_myaccount_page_id" ,
                                        "woocommerce_edit_address_page_id" ,
                                        "woocommerce_view_order_page_id" ,
                                        "woocommerce_change_password_page_id" ,
                                        "woocommerce_logout_page_id" ,
                                        "woocommerce_lost_password_page_id" ) ;
        foreach ( $woocommerce_keys as $wc_page_id ) {
                if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
                        return true ;
                }
        }
        return false;
    }
}

//navigation
if(!function_exists('s7upf_paging_nav'))
{
    function s7upf_paging_nav()
    {
        // Don't print empty markup if there's only one page.
        if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
            return;
        }

        $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $query_args   = array();
        $url_parts    = explode( '?', $pagenum_link );

        if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
        }

        $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
        $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

        $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

        // Set up paginated links.
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $GLOBALS['wp_query']->max_num_pages,
            'current'  => $paged,
            'mid_size' => 1,
            'type'     => 'list',
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        ) );

        if ($links) : ?>
        <div class="sort-pagi-bar bottom clearfix">
            <?php echo balanceTags($links); ?>
        </div>
        <?php endif;
    }
}

//Set post view
if(!function_exists('s7upf_set_post_view'))
{
    function s7upf_set_post_view($post_id=false)
    {
        if(!$post_id) $post_id=get_the_ID();

        $view=(int)get_post_meta($post_id,'post_views',true);
        $view++;
        update_post_meta($post_id,'post_views',$view);
    }
}

if(!function_exists('s7upf_get_post_view'))
{
    function s7upf_get_post_view($post_id=false)
    {
        if(!$post_id) $post_id=get_the_ID();

        return (int)get_post_meta($post_id,'post_views',true);
    }
}

//remove attr embed
if(!function_exists('s7upf_remove_w3c')){
    function s7upf_remove_w3c($embed_code){
        $embed_code=str_replace('webkitallowfullscreen','',$embed_code);
        $embed_code=str_replace('mozallowfullscreen','',$embed_code);
        $embed_code=str_replace('frameborder="0"','',$embed_code);
        $embed_code=str_replace('frameborder="no"','',$embed_code);
        $embed_code=str_replace('scrolling="no"','',$embed_code);
        $embed_code=str_replace('&','&amp;',$embed_code);
        return $embed_code;
    }
}

// MetaBox
if(!function_exists('s7upf_display_metabox'))
{
    function s7upf_display_metabox($type ='') {
        $archive_year  = get_the_time('Y'); 
        $archive_month = get_the_time('m'); 
        $archive_day   = get_the_time('d');
        ?>
        <ul class="post-comment-date list-none">
            <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day)?>"><span><?php echo get_the_date('M d Y')?></span></a></li>
            <li><i class="fa fa-comment-o" aria-hidden="true"></i><span><?php echo get_comments_number(); ?> 
                <?php 
                    if(get_comments_number()>1) esc_html_e('Comments', 'lucky') ;
                    else esc_html_e('Comment', 'lucky') ;
                ?>
            </span></li>
            <?php if(is_front_page() && is_home()):?>
                <li><i class="fa fa-tags" aria-hidden="true"></i>
                <span><?php esc_html_e('In:', 'lucky');?></span>
                    <?php $cats = get_the_category_list(', ');?>
                    <?php if($cats) echo balanceTags($cats); else _e("No Category",'lucky');?>
                </li>
                <li class="sv_post_tag"><i class="fa fa-tags" aria-hidden="true"></i>
                    <span><?php esc_html_e('Tags:', 'lucky');?></span>
                    <?php $tags = get_the_tag_list(' ',', ',' ');?>
                    <?php if($tags) echo balanceTags($tags); else esc_html_e("No Tag",'lucky');?>
                </li>
            <?php endif;?>
        </ul>
    <?php
    }
}
if(!function_exists('s7upf_get_header_default')){
    function s7upf_get_header_default(){
        ?>
        <div id="header" class="header header-default">
            <div class="container">
                <div class="main-header">
                    <div class="logo logo1">
                        <?php $s7upf_logo=s7upf_get_option('logo');?>
                        <?php if($s7upf_logo!=''){
                            echo '<h1 class="hidden">'.get_bloginfo('name', 'display').'</h1><a href="'.esc_url(get_home_url('/')).'"><img src="' . esc_url($s7upf_logo) . '" alt="logo"></a>';
                        }   else { echo '<h1><a href="'.esc_url(get_home_url('/')).'">'.get_bloginfo('name', 'display').'</a></h1>'; }
                        ?>
                    </div>
                    <nav class="main-nav main-nav1">
                        <?php if ( has_nav_menu( 'primary' ) ) {
                            wp_nav_menu( array(
                                    'theme_location' => 'primary',
                                    'container'=>false,
                                    'walker'=>new S7upf_Walker_Nav_Menu(),
                                 )
                            );
                        } ?>
                        <a href="#" class="toggle-mobile-menu"><span></span></a>
                    </nav>
                </div>
            </div>
        </div>
        <?php
    }
}
if(!function_exists('s7upf_get_footer_default')){
    function s7upf_get_footer_default(){
        ?>
        <div id="footer" class="default-footer footer-bottom1">
            <div class="container">
                <p class="copyright"><?php esc_html_e("Copyright &copy; by 7up. All Rights Reserved. Designed by","lucky")?> <a href="#"><span><?php esc_html_e("7uptheme","lucky")?></span>.<?php esc_html_e("com","lucky")?></a>.</p>
            </div>
        </div>
        <?php
    }
}
if(!function_exists('s7upf_get_footer_visual')){
    function s7upf_get_footer_visual($page_id){
        ?>
        <div id="footer" class="footer-page">
            <div class="container">
                <?php echo S7upf_Template::get_vc_pagecontent($page_id);?>
            </div>
        </div>
        <?php
    }
}
if(!function_exists('s7upf_get_header_visual')){
    function s7upf_get_header_visual($page_id){
        ?>
        <div id="header" class="header header-page">
            <div class="container">
                <?php echo S7upf_Template::get_vc_pagecontent($page_id);?>
            </div>
        </div>
        <?php
    }
}
if(!function_exists('s7upf_get_main_class')){
    function s7upf_get_main_class(){
        $sidebar=s7upf_get_sidebar();
        $sidebar_pos=$sidebar['position'];
        $main_class = 'col-md-12';
        if($sidebar_pos != 'no') $main_class = 'col-md-9 col-sm-8 col-xs-12';
        return $main_class;
    }
}
if(!function_exists('s7upf_output_sidebar')){
    function s7upf_output_sidebar($position){
        $sidebar = s7upf_get_sidebar();
        $sidebar_pos = $sidebar['position'];
        if($sidebar_pos == $position) get_sidebar();
    }
}
if(!function_exists('s7upf_fix_import_category')){
    function s7upf_fix_import_category($taxonomy){
        global $s7upf_config;
        $data = $s7upf_config['import_category'];
        if(!empty($data)){
            $data = json_decode($data,true);
            foreach ($data as $cat => $value) {
                $parent_id = 0;
                $term = get_term_by( 'slug',$cat, $taxonomy );
                $term_parent = get_term_by( 'slug', $value['parent'], $taxonomy );
                if(isset($term_parent->term_id)) $parent_id = $term_parent->term_id;
                if($parent_id) wp_update_term( $term->term_id, $taxonomy, array('parent'=> $parent_id) );
                if($value['thumbnail']){
                    if($taxonomy == 'product_cat')  update_metadata( 'woocommerce_term', $term->term_id, 'thumbnail_id', $value['thumbnail']);
                    else{
                        update_term_meta( $term->term_id, 'thumbnail_id', $value['thumbnail']);
                    }
                }
            }
        }
    }
}
if ( ! function_exists( 's7upf_get_google_link' ) ) {
    function s7upf_get_google_link() {
        $protocol = is_ssl() ? 'https' : 'http';
        $fonts_url = '';
        $fonts  = array(
                    'Open Sans:400,300,700'
                );
        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
            ), $protocol.'://fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }
}
/***************************************END Core Function***************************************/


/***************************************Add Theme Function***************************************/
// Mini cart
if(!function_exists('s7upf_mini_cart')){
    function s7upf_mini_cart($echo = false){
        $html = '';
        if ( ! WC()->cart->is_empty() ){
            $count_item = 0; $html = '';
            $html .=  '<ul class="info-list-cart">';                    
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $count_item++;
                $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                $product_quantity = woocommerce_quantity_input( array(
                    'input_name'  => "cart[{$cart_item_key}][qty]",
                    'input_value' => $cart_item['quantity'],
                    'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                    'min_value'   => '0'
                ), $_product, false );
                $thumb_html = '';
                if(has_post_thumbnail($product_id)) $thumb_html = $_product->get_image(array(70,70));
                $html .=    '<li class="item-info-cart" data-key="'.$cart_item_key.'">
                                <div class="cart-thumb">
                                    <a href="'.esc_url( $_product->get_permalink( $cart_item )).'" class="cart-thumb">
                                        '.$thumb_html.'
                                    </a>
                                </div>  
                                <div class="wrap-cart-title">
                                    <h3 class="cart-title"><a href="'.esc_url( $_product->get_permalink( $cart_item )).'">'.$_product->get_title().'</a></h3>
                                    <div class="cart-qty"><label>'.esc_html__("Qty","lucky").':</label> <span>'.$cart_item['quantity'].'</span></div>
                                </div>  
                                <div class="wrap-cart-remove">
                                    <a href="#" class="remove-product btn-remove"></a>
                                    <span class="cart-price">'.apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ).'</span>
                                </div>  
                            </li>';
            }
            $html .=    '</ul><input id="count-cart-item" type="hidden" value="'.$count_item.'">';
            $html .=    '<div class="total-cart">
                            <label>'.esc_html__("Subtotal","lucky").'</label> <span class="total-price">'.WC()->cart->get_cart_total().'</span>
                        </div>
                        <div class="link-cart">
                            <a href="'.wc_get_cart_url().'" class="cart-edit">'.esc_html__("edit cart","lucky").'</a>
                            <a href="'.wc_get_checkout_url().'" class="cart-checkout">'.esc_html__("checkout","lucky").'</a>
                        </div>';
        }
        else $html .= '<ul class="info-list-cart"><li class="item-info-cart"><h3>'.esc_html__("No product in cart","lucky").'</h3></li></ul>';
        if($echo) echo balanceTags($html);
        else return $html;
    }
}
if(!function_exists('s7upf_get_list_animation'))
{
    function s7upf_get_list_animation() {
        $list = array(
            esc_html__('None','lucky')                   => '',
            esc_html__('bounce','lucky')                 => 'bounce',
            esc_html__('flash','lucky')                  => 'flash',
            esc_html__('pulse','lucky')                  => 'pulse',
            esc_html__('rubberBand','lucky')             => 'rubberBand',
            esc_html__('shake','lucky')                  => 'shake',
            esc_html__('headShake','lucky')              => 'headShake',
            esc_html__('swing','lucky')                  => 'swing',
            esc_html__('tada','lucky')                   => 'tada',
            esc_html__('wobble','lucky')                 => 'wobble',
            esc_html__('jello','lucky')                  => 'jello',
            esc_html__('bounceIn','lucky')               => 'bounceIn',
            esc_html__('bounceInDown','lucky')           => 'bounceInDown',
            esc_html__('bounceInLeft','lucky')           => 'bounceInLeft',
            esc_html__('bounceInRight','lucky')          => 'bounceInRight',
            esc_html__('bounceInUp','lucky')             => 'bounceInUp',
            esc_html__('bounceOut','lucky')              => 'bounceOut',
            esc_html__('bounceOutDown','lucky')          => 'bounceOutDown',
            esc_html__('bounceOutLeft','lucky')          => 'bounceOutLeft',
            esc_html__('bounceOutRight','lucky')         => 'bounceOutRight',
            esc_html__('bounceOutUp','lucky')            => 'bounceOutUp',
            esc_html__('fadeIn','lucky')                 => 'fadeIn',
            esc_html__('fadeInDown','lucky')             => 'fadeInDown',
            esc_html__('fadeInDownBig','lucky')          => 'fadeInDownBig',
            esc_html__('fadeInLeft','lucky')             => 'fadeInLeft',
            esc_html__('fadeInLeftBig','lucky')          => 'fadeInLeftBig',
            esc_html__('fadeInRight','lucky')            => 'fadeInRight',
            esc_html__('fadeInRightBig','lucky')         => 'fadeInRightBig',
            esc_html__('fadeInUp','lucky')               => 'fadeInUp',
            esc_html__('fadeInUpBig','lucky')            => 'fadeInUpBig',
            esc_html__('fadeOut','lucky')                => 'fadeOut',
            esc_html__('fadeOutDown' ,'lucky')           => 'fadeOutDown',
            esc_html__('fadeOutDownBig','lucky')         => 'fadeOutDownBig',
            esc_html__('fadeOutLeft','lucky')            => 'fadeOutLeft',
            esc_html__('fadeOutLeftBig','lucky')         => 'fadeOutLeftBig',
            esc_html__('fadeOutRight','lucky')           => 'fadeOutRight',
            esc_html__('fadeOutRightBig','lucky')        => 'fadeOutRightBig',
            esc_html__('fadeOutUp','lucky')              => 'fadeOutUp',
            esc_html__('fadeOutUpBig','lucky')           => 'fadeOutUpBig',
            esc_html__('flipInX','lucky')                => 'flipInX',
            esc_html__('flipInY','lucky')                => 'flipInY',
            esc_html__('flipOutX','lucky')               => 'flipOutX',
            esc_html__('flipOutY','lucky')               => 'flipOutY',
            esc_html__('lightSpeedIn','lucky')           => 'lightSpeedIn',
            esc_html__('lightSpeedOut','lucky')          => 'lightSpeedOut',
            esc_html__('rotateIn','lucky')               => 'rotateIn',
            esc_html__('rotateInDownLeft','lucky')       => 'rotateInDownLeft',
            esc_html__('rotateInDownRight','lucky')      => 'rotateInDownRight',
            esc_html__('rotateInUpLeft','lucky')         => 'rotateInUpLeft',
            esc_html__('rotateInUpRight','lucky')        => 'rotateInUpRight',
            esc_html__('rotateOut','lucky')              => 'rotateOut',
            esc_html__('rotateOutDownLeft','lucky')      => 'rotateOutDownLeft',
            esc_html__('rotateOutDownRight','lucky')     => 'rotateOutDownRight',
            esc_html__('rotateOutUpLeft','lucky')        => 'rotateOutUpLeft',
            esc_html__('rotateOutUpRight','lucky')       => 'rotateOutUpRight',
            esc_html__('hinge','lucky')                  => 'hinge',
            esc_html__('rollIn','lucky')                 => 'rollIn',
            esc_html__('rollOut','lucky')                => 'rollOut',
            esc_html__('zoomIn','lucky')                 => 'zoomIn',
            esc_html__('zoomInDown','lucky')             => 'zoomInDown',
            esc_html__('zoomInLeft','lucky')             => 'zoomInLeft',
            esc_html__('zoomInRight','lucky')            => 'zoomInRight',
            esc_html__('zoomInUp','lucky')               => 'zoomInUp',
            esc_html__('zoomOut','lucky')                => 'zoomOut',
            esc_html__('zoomOutDown','lucky')            => 'zoomOutDown',
            esc_html__('zoomOutLeft','lucky')            => 'zoomOutLeft',
            esc_html__('zoomOutRight','lucky')           => 'zoomOutRight',
            esc_html__('zoomOutUp','lucky')              => 'zoomOutUp',
            esc_html__('slideInDown','lucky')            => 'slideInDown',
            esc_html__('slideInLeft','lucky')            => 'slideInLeft',
            esc_html__('slideInRight','lucky')           => 'slideInRight',
            esc_html__('slideInUp','lucky')              => 'slideInUp',
            esc_html__('slideOutDown','lucky')           => 'slideOutDown',
            esc_html__('slideOutLeft','lucky')           => 'slideOutLeft',
            esc_html__('slideOutRight','lucky')          => 'slideOutRight',
            esc_html__('slideOutUp','lucky')             => 'slideOutUp',
            );
        return $list;
    }
}
//product main detail
if(!function_exists('s7upf_product_main_detai')){
    function s7upf_product_main_detai($ajax = false){
        global $post, $product, $woocommerce;
        s7upf_set_post_view();
        $thumb_id = array(get_post_thumbnail_id());
        $attachment_ids = $product->get_gallery_image_ids();
        $attachment_ids = array_merge($thumb_id,$attachment_ids);
        $attachment_ids = array_unique($attachment_ids);
        $ul_block = $bx_block = '';$i = 0;
        $available_data = array();
        if( $product->is_type( 'variable' ) ) $available_data = $product->get_available_variations();
        foreach ( $attachment_ids as $attachment_id ) {
            $image_link = wp_get_attachment_url( $attachment_id );
            if ( ! $image_link )
                continue;
            $image_title    = esc_attr( get_the_title( $attachment_id ) );
            $image_caption  = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
            $image       = wp_get_attachment_image( $attachment_id, 'full', 0, $attr = array(
                'title' => $image_title,
                'alt'   => $image_title
                ) );
            $image_pager       = wp_get_attachment_image( $attachment_id, array(100,125), 0, $attr = array(
                'title' => $image_title,
                'alt'   => $image_title
                ) );
            $ul_block .= '<li>'.$image.'</li>';
            $bx_block .= '<a data-image_id="'.esc_attr($attachment_id).'" data-slide-index="'.$i.'" href="#">'.$image_pager.'</a>';
            $i++;
        }
        if(!empty($available_data)){
            foreach ($available_data as $available) {
                if(!empty($available['image_id']) && !in_array($available['image_id'],$attachment_ids)){
                    $attachment_ids[] = $available['image_id'];
                    if(!empty($available['image_id'])){
                        $image2 = wp_get_attachment_image( $available['image_id'], 'full', 0, $attr = array(
                        'title' => $image_title,
                        'alt'   => $image_title
                        ) );
                        $image_pager2       = wp_get_attachment_image( $available['image_id'], array(100,125), 0, $attr = array(
                        'title' => $image_title,
                        'alt'   => $image_title
                        ) );
                        $ul_block .= '<li>'.$image2.'</li>';
                        $bx_block .= '<a data-image_id="'.esc_attr($available['image_id']).'" data-slide-index="'.$i.'" href="#">'.$image_pager2.'</a>';
                        $i++;
                    }
                }
            }
        }
        $sku = get_post_meta(get_the_ID(),'_sku',true);
        $stock = $product->get_availability();
        $s_class = '';
        if(is_array($stock)){
            if(!empty($stock['class'])) $s_class = $stock['class'];
            if(!empty($stock['availability'])) $stock = $stock['availability'];
            else {
                if($stock['class'] == 'in-stock') $stock = esc_html__("In stock","lucky");
                else $stock = esc_html__("Out of stock","lucky");
            }
        }
        ?>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="product-gallery">
                        <div class="clearfix">
                            <ul class="bxslider">
                                <?php echo balanceTags($ul_block)?>
                            </ul>
                            <div id="bx-pager">
                                <?php echo balanceTags($bx_block)?>
                            </div>
                        </div>
                        <ul class="list-inline share-social">
                            <li><a href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink();?>" class="share-face"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="http://www.twitter.com/share?url=<?php echo get_the_permalink();?>" class="share-twit"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="share-pint"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <!-- End Product Gallery -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-product-detail">
                        <h2 class="title-product-detail"><?php the_title()?></h2>
                        <ul class="list-inline review-rating">
                            <li>
                                <?php echo s7upf_get_rating_html(true,'rate-counter');?>
                            </li>
                            <li>
                                <a href="#" class="add-review"><?php esc_html_e("Add your review","lucky")?></a>
                            </li>
                        </ul>
                        <?php echo s7upf_get_price_html();?>
                        <p class="desc"><?php echo get_the_excerpt();?></p>
                        <div class="info-extra">
                            <?php
                                echo s7upf_wishlist_link();
                                echo s7upf_compare_url();
                            ?>
                        </div>
                        <div class="wrap-cart-qty">
                            <?php do_action('s7upf_template_single_add_to_cart');?>
                        </div>
                        <div class="product-available" data-instock="<?php esc_attr_e("In stock","lucky")?>" data-outstock="<?php esc_attr_e("Out of stock","lucky")?>">
                            <span><?php esc_html_e("Available:","lucky")?> <span class="avail-instock <?php echo esc_attr($s_class)?>"><?php echo esc_html($stock)?></span></span>
                        </div>
                        <p class="desc product-code"><?php esc_html_e("#Code Products:","lucky")?> <?php echo '<span>'.esc_html($sku).'</span>'?></p>
                        <?php do_action( 'woocommerce_product_meta_end' );?>
                    </div>
                </div>
            </div>
        <?php   
    }
}
if(!function_exists('s7upf_substr')){
    function s7upf_substr($string='',$start=0,$end=1){
        $output = '';
        if(!empty($string)){
            if($end < strlen($string)){
                if($string[$end] != ' '){
                    for ($i=$end; $i < strlen($string) ; $i++) { 
                        if($string[$i] == ' ' || $string[$i] == '.' || $i == strlen($string)-1){
                            $end = $i;
                            break;
                        }
                    }
                }
            }
            $output = substr($string,$start,$end);
        }
        return $output;
    }
}
//get type url
if(!function_exists('s7upf_get_key_url')){
    function s7upf_get_key_url($key,$value){
        if(function_exists('s7upf_get_current_url')) $current_url = s7upf_get_current_url();
        else{
            if(function_exists('woocommerce_get_page_id')) $current_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
            else $current_url = get_permalink();
        }
        if(isset($_GET[$key])){
            $current_url = str_replace('&'.$key.'='.$_GET[$key], '', $current_url);
            $current_url = str_replace('?'.$key.'='.$_GET[$key], '', $current_url);
        }
        if(strpos($current_url,'?') > -1 ){
            $current_url .= '&amp;'.$key.'='.$value;
        }
        else {
            $current_url .= '?'.$key.'='.$value;
        }
        return $current_url;
    }
}
if(!function_exists('s7upf_get_rating_html')){
    function s7upf_get_rating_html($count = false,$style = ''){
        global $product;
        $html = '';
        $star = $product->get_average_rating();
        $review_count = $product->get_review_count();
        $width = $star / 5 * 100;
        $html .=    '<div class="product-rate '.esc_attr($style).'">
                        <div class="product-rating" style="width:'.$width.'%;"></div>';
        if($count) $html .= '<span>('.$review_count.'s)</span>';
        $html .=    '</div>';
        return $html;
    }
}
if ( ! function_exists( 's7upf_addtocart_link' ) ) {
    function s7upf_addtocart_link($product = false){
        if(!$product) global $product;
        $icon = '<i class="fa fa-opencart" aria-hidden="true"></i>';
        $text = $product->add_to_cart_text();
        $btn_class = 'addcart-link';
        $button_html =  apply_filters( 'woocommerce_loop_add_to_cart_link',
            sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="%s product_type_%s">'.$icon.'<span>%s</span></a>',
                esc_url( $product->add_to_cart_url() ),
                esc_attr( $product->get_id() ),
                esc_attr( $product->get_sku() ),
                esc_attr( isset( $quantity ) ? $quantity : 1 ),
                $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button '.$btn_class : $btn_class,
                esc_attr( $product->get_type() ),
                esc_html( $text )
            ),
        $product );
        if($product) $button_html = str_replace('wp-admin/admin-ajax.php', '', $button_html);
        return $button_html;
    }
}
//Compare URL
if(!function_exists('s7upf_compare_url')){
    function s7upf_compare_url($id = false){
        global $yith_woocompare;
        if(!$id) $id = get_the_ID();
        if(class_exists('YITH_Woocompare')){
            $icon = '<i class="fa fa-file-o" aria-hidden="true"></i>';
            $cp_link = str_replace('&', '&amp;',add_query_arg( array('action' => 'yith-woocompare-add-product','id' => $id )));
            $html = '<a href="'.esc_url($cp_link).'" class="product-compare compare compare-link" data-product_id="'.esc_attr($id).'">'.$icon.'<span>'.esc_html__("Compare","lucky").'</span></a>';
            return $html;
        }
    }
}
if ( ! function_exists( 's7upf_wishlist_link' ) ) {
    function s7upf_wishlist_link($id = false){
        if(!$id) $id = get_the_ID();
        $html_wl = '';
        if(class_exists('YITH_WCWL_Init')) $html_wl = '<a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'add_to_wishlist', $id ))).'" class="add_to_wishlist wishlist-link" rel="nofollow" data-product-id="'.esc_attr($id).'" data-product-title="'.esc_attr(get_the_title($id)).'"><i class="fa fa-heart-o" aria-hidden="true"></i><span>'.esc_html__("Wishlist","lucky").'</span></a>';
        return $html_wl;
    }
}
if ( ! function_exists( 's7upf_product_link' ) ) {
    function s7upf_product_link($style=''){
        $html = $html_wl = '';
        if(class_exists('YITH_WCWL_Init')) $html_wl = '<a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'add_to_wishlist', get_the_ID() ))).'" class="add_to_wishlist wishlist-link" rel="nofollow" data-product-id="'.get_the_ID().'" data-product-title="'.esc_attr(get_the_title()).'"><i class="fa fa-heart-o" aria-hidden="true"></i><span>'.esc_html__("Wishlist","lucky").'</span></a>';
        switch ($style) {
            case 'deal-home11':
                $html .=     '<div class="product-extra-link11 product">';
                $html .=        s7upf_addtocart_link();
                $html .=        s7upf_compare_url();
                $html .=        $html_wl;
                $html .=    '</div>';
                break;
            
            default:
                $html .=     '<div class="product-extra-link hidden-text product">';
                $html .=        s7upf_compare_url();
                $html .=        s7upf_addtocart_link();
                $html .=        $html_wl;
                $html .=    '</div>';
                break;
        }
        return $html;
    }
}
if(!function_exists('s7upf_get_price_html')){
    function s7upf_get_price_html($style = ''){
        global $product;
        switch ($style) {
            case 'style2':
                $html =    '<div class="price-style2">'.$product->get_price_html().'</div>';
                break;
            
            default:                
                $html =    $product->get_price_html();
                break;
        }
        return $html;
    }
}
if(!function_exists('s7upf_get_rating_html')){
    function s7upf_get_rating_html($count = false,$style = ''){
        global $product;
        $html = '';
        $star = $product->get_average_rating();
        $review_count = $product->get_review_count();
        $width = $star / 5 * 100;
        $html .=    '<div class="product-rate '.esc_attr($style).'">
                        <div class="product-rating" style="width:'.$width.'%;"></div>';
        if($count) $html .= '<span>('.$review_count.'s)</span>';
        $html .=    '</div>';
        return $html;
    }
}
// get list taxonomy
if(!function_exists('s7upf_list_taxonomy'))
{
    function s7upf_list_taxonomy($taxonomy,$show_all = true)
    {
        if($show_all) $list = array('--Select--' => '');
        else $list = array();
        if(!isset($taxonomy) || empty($taxonomy)) $taxonomy = 'category';
        $tags = get_terms($taxonomy);
        if(is_array($tags) && !empty($tags)){
            foreach ($tags as $tag) {
                $list[$tag->name] = $tag->slug;
            }
        }
        return $list;
    }
}
if(!function_exists('s7upf_get_saleoff_html')){
    function s7upf_get_saleoff_html($style=''){
        global $product,$post;
        $from = $product->get_regular_price();
        $to = $product->get_price();
        $date_pro = strtotime($post->post_date);
        $date_now = strtotime('now');
        $set_timer = s7upf_get_option( 'sv_set_time_woo', 30);
        $uppsell = ($date_now - $date_pro - $set_timer*24*60*60);
        $percent = $html = '';
        if($from != $to && $from > 0){
            $percent = round(($from-$to)/$from*100);            
            $html .= '<span class="product-label sale-label">-'.$percent.'%</span>';
        }
        if($uppsell < 0) $html .=  '<span class="product-label new-label">'.esc_html__("new","lucky").'</span>';
        return $html;
    }
}
if(!function_exists('s7upf_header_image')){
    function s7upf_header_image(){        
        $header_show = s7upf_get_value_by_id('show_header_page');
        $header_images = s7upf_get_value_by_id('header_page_image');
        $html = '';
        if($header_show == 'on' && !is_single()){            
            if(function_exists('is_shop')) $is_shop = is_shop();
            else $is_shop = false;           
            if(is_archive() && !$is_shop){
                global $wp_query;
                $term = $wp_query->get_queried_object();
                if(is_object($term)){
                    $header_images = array();
                    $image = get_term_meta($term->term_id, 'cat-header-image', true);
                    $link = get_term_meta($term->term_id, 'cat-header-link', true);
                    if(!empty($image)) $header_images[0]['header_image'] = $image;
                    if(!empty($link)) $header_images[0]['header_link'] = $link;
                }
            }
            $html .=    '<div class="content-shoptop content-top1">
                            <div class="container">
                                <div class="banner-slider banner-slider1 banner-shop">
                                    <div class="wrap-item smart-slider" data-item="1" data-speed="" data-itemres="" data-prev="" data-next="" data-pagination="" data-navigation="true">';
            if(!empty($header_images) && is_array($header_images)){
                foreach ($header_images as $item) {
                    $html .=    '<div class="item-slider">
                                    <div class="banner-thumb"><a href="'.esc_url($item['header_link']).'"><img src="'.esc_url($item['header_image']).'" alt=""></a></div>
                                </div>';
                }
            }
            $html .=                '</div>
                                </div>
                            </div>
                        </div>';
        }
        echo balanceTags($html);
    }
}
if(!function_exists('s7upf_check_sidebar')){
    function s7upf_check_sidebar(){
        $sidebar = s7upf_get_sidebar();
        if($sidebar['position'] == 'no') return false;
        else return true;
    }
}
if(!function_exists('s7upf_get_class_sidebar')){
    function s7upf_get_class_sidebar(){
        if(s7upf_check_sidebar()) return 'has-sidebar';
        else return 'no-sidebar';
    }
}
if(!function_exists('s7upf_single_product_tabs')){
    function s7upf_single_product_tabs(){
        $check_upsell = s7upf_get_value_by_id('show_single_upsell');
        $check_latest = s7upf_get_value_by_id('show_single_lastest');
        $check_relate = s7upf_get_value_by_id('show_single_relate');
        $number = s7upf_get_value_by_id('show_single_number');
        if(!$number) $number = 6; 
        $item = 3;
        if(!s7upf_check_sidebar()) $item = 4;
        global $product;
        $upsells = $product->get_upsell_ids();
        $related = wc_get_related_products($product->get_id(),$number);
        $html = '';
        if($check_upsell == 'on' || $check_latest == 'on' || $check_relate == 'on'){
            $html .=    '<div class="relate-product">
                            <div class="nav-tabs-default">
                                <ul class="nav nav-tabs" role="tablist">';
            if($check_relate == 'on') $html .=    '<li role="presentation" class="active"><a href="#relate-product" aria-controls="related product" role="tab" data-toggle="tab">'.esc_html__("Related Products","lucky").'</a></li>';
            if($check_upsell == 'on') $html .=    '<li role="presentation" class=""><a href="#upsell-product" aria-controls="related product" role="tab" data-toggle="tab">'.esc_html__("UP-SELL PRODUCTS","lucky").'</a></li>';
            if($check_latest == 'on') $html .=    '<li role="presentation" class=""><a href="#latest-product" aria-controls="related product" role="tab" data-toggle="tab">'.esc_html__("Latest Products","lucky").'</a></li>';
            $html .=            '</ul>
                            </div>
                            <div class="tab-content">';
            if($check_relate == 'on'){
                $html .=    '<div role="tabpanel" class="tab-pane active" id="related-product">
                                <div class="product-related-slider">
                                    <div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="" data-itemres="" data-prev="" data-next="" data-pagination="" data-navigation="true">';
                $args = array(
                    'post_type'           => 'product',
                    'ignore_sticky_posts'  => 1,
                    'no_found_rows'        => 1,
                    'posts_per_page'       => $number,                                    
                    'orderby'              => 'ID',
                    'post__in'             => $related,
                    'post__not_in'         => array( $product->get_id() )
                );
                $products = new WP_Query( $args );
                if ( $products->have_posts() ) {
                    while ( $products->have_posts() ) { 
                        $products->the_post();
                        $html .=    '<div class="item-product">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link ff">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            '.s7upf_get_rating_html().'
                                            '.s7upf_get_saleoff_html().'
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
            }
            if($check_upsell == 'on'){
                $html .=    '<div role="tabpanel" class="tab-pane" id="upsell-product">
                                <div class="product-related-slider">
                                    <div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="" data-itemres="" data-prev="" data-next="" data-pagination="" data-navigation="true">';
                $meta_query = WC()->query->get_meta_query();
                $args = array(
                    'post_type'           => 'product',
                    'ignore_sticky_posts' => 1,
                    'no_found_rows'       => 1,
                    'posts_per_page'      => $number,
                    'post__in'            => $upsells,
                    'post__not_in'        => array( $product->get_id() ),
                    'meta_query'          => $meta_query
                );
                $products = new WP_Query( $args );
                if ( $products->have_posts() ) {
                    while ( $products->have_posts() ) { 
                        $products->the_post();
                        $html .=    '<div class="item-product">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link xx">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            '.s7upf_get_rating_html().'
                                            '.s7upf_get_saleoff_html().'
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
            }
            if($check_latest == 'on'){
                $html .=    '<div role="tabpanel" class="tab-pane " id="latest-product">
                                <div class="product-related-slider">
                                    <div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="" data-itemres="" data-prev="" data-next="" data-pagination="" data-navigation="true">';
                $args = array(
                    'post_type'           => 'product',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page'      => $number,
                    'post__not_in'        => array( get_the_ID() ),
                    'orderby'             => 'date'
                );
                $products = new WP_Query( $args );
                if ( $products->have_posts() ) {
                    while ( $products->have_posts() ) { 
                        $products->the_post();
                        $html .=    '<div class="item-product">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link gg">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            '.s7upf_get_rating_html().'
                                            '.s7upf_get_saleoff_html().'
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
            }
            $html .=        '</div>
                        </div>';
            wp_reset_postdata();
        }
        echo balanceTags($html);
    }
}
//get type url
if(!function_exists('s7upf_get_filter_url')){
    function s7upf_get_filter_url($key,$value){
        if(function_exists('s7upf_get_current_url')) $current_url = s7upf_get_current_url();
        else{
            if(function_exists('woocommerce_get_page_id')) $current_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
            else $current_url = get_permalink();
        }
        if(isset($_GET[$key])){
            $current_val_string = $_GET[$key];
            if($current_val_string == $value){
                $current_url = str_replace('&'.$key.'='.$_GET[$key], '', $current_url);
                $current_url = str_replace('?'.$key.'='.$_GET[$key], '?', $current_url);
            }
            $current_val_key = explode(',', $current_val_string);
            $val_encode = str_replace(',', '%2C', $current_val_string);
            if(!empty($current_val_string)){
                if(!in_array($value, $current_val_key)) $current_val_key[] = $value;
                else{
                    $pos = array_search($value, $current_val_key);
                    unset($current_val_key[$pos]);
                }            
                $new_val_string = implode('%2C', $current_val_key);
                $current_url = str_replace($key.'='.$val_encode, $key.'='.$new_val_string, $current_url);
                if (strpos($current_url, '?') == false) $current_url = str_replace('&','?',$current_url);
            }
            else $current_url = str_replace($key.'=', $key.'='.$value, $current_url);     
        }
        else{
            if(strpos($current_url,'?') > -1 ){
                $current_url .= '&amp;'.$key.'='.$value;
            }
            else {
                $current_url .= '?'.$key.'='.$value;
            }
        }
        return $current_url;
    }
}
if(!function_exists('s7upf_scroll_top')){
    function s7upf_scroll_top(){
        $scroll_top = s7upf_get_value_by_id('show_scroll_top');
        if($scroll_top == 'on'):?>
        <a href="#" class="scroll-top radius"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        <?php endif;
    }
}
if(!function_exists('s7upf_product_filter_attr')){
    function s7upf_product_filter_attr($cat = true){
        $html = '';
        $html .=    '<div class="box-attr-filter">';
        if($cat){
            $html .=        '<div class="item-box-attr">
                                <div class="item-attr-title">
                                    <label>'.esc_html__("Categories","lucky").'</label>
                                </div>
                                <div class="item-attr-content">
                                    <ul class="attr-list attr-category list-inline" data-key="category">';
            $cats = get_terms('product_cat');
            foreach ($cats as $cat) {            
                if(is_object($cat)) $html .=    '<li><a href="#" class="filter-ajax" data-value="'.$cat->slug.'">'.$cat->name.'</a></li>';  
            }
            $html .=                '</ul>
                                </div>
                            </div>';
        }
        $price_arange = array();
        if(function_exists('s7upf_get_price_arange')) $price_arange = s7upf_get_price_arange();
        if(!empty($price_arange)){
            $min = $price_arange['min'];
            $max = $price_arange['max'];
            $html .=        '<div class="item-box-attr">
                                <div class="item-attr-title">
                                    <label>'.esc_html__("Price","lucky").'</label>
                                </div>
                                <div class="item-attr-content">
                                    <div class="slider-range-price" data-pmax="'.$max.'" data-pmin="'.$min.'" data-currency="'.get_woocommerce_currency_symbol().'"></div>
                                    <div class="attr-price-filter">
                                        <p>
                                            <label>'.esc_html__("Filter By Price","lucky").': </label>
                                            <input type="text" class="amount-price" readonly />
                                            <input type="hidden" class="get-filter-price filter-ajax" />
                                        </p>
                                    </div>
                                </div>
                            </div>';
        }
        $attribute_taxonomies = array();
        if(function_exists('wc_get_attribute_taxonomies')) $attribute_taxonomies = wc_get_attribute_taxonomies();
        if(!empty($attribute_taxonomies)){
            foreach($attribute_taxonomies as $attr){
                $html .=    '<div class="item-box-attr">
                                <div class="item-attr-title">
                                    <label>'.$attr->attribute_label.'</label>
                                </div>
                                <div class="item-attr-content">
                                    <ul class="attr-list attr-style attr-'.$attr->attribute_name.' list-inline" data-key="'.$attr->attribute_name.'">';
                $terms = get_terms("pa_".$attr->attribute_name);
                if(is_array($terms)){
                    foreach ($terms as $term) {                        
                        $attr_label = $term->name;
                        $html .=        '<li><a href="#" class="filter-ajax termbg-'.$term->slug.'" data-value="'.$term->slug.'"><span></span>'.$attr_label.'</a></li>';
                    }
                }                
                $html .=            '</ul>
                                </div>
                            </div>';
            }
        }        
        $html .=     '</div>';
        return $html;
    }
}
if(!function_exists('s7upf_filter_price')){
    function s7upf_filter_price($min,$max,$filtered_posts = array()){
        global $wpdb;
        $matched_products = array( 0 );
        $matched_products_query = apply_filters( 'woocommerce_price_filter_results', $wpdb->get_results( $wpdb->prepare("
            SELECT DISTINCT ID, post_parent, post_type FROM $wpdb->posts
            INNER JOIN $wpdb->postmeta ON ID = post_id
            WHERE post_type IN ( 'product', 'product_variation' ) AND post_status = 'publish' AND meta_key = %s AND meta_value BETWEEN %d AND %d
        ", '_price', $min, $max ), OBJECT_K ), $min, $max );

        if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
        }
        return $filtered_posts;
    }
}
//List Post
if(!function_exists('s7upf_get_list_post')){
    function s7upf_get_list_post(){
        $list = array();
        $list[] = array(
            'value' => '',
            'label' => esc_html__('-- Choose One --','lucky')
        );
        $args= array(
        'post_type' => 'post',
        'posts_per_page' => 50, 
        );
        $query = new WP_Query($args);
        global $post;
        if($query->have_posts()): while ($query->have_posts()):$query->the_post();            
            $list[] = array(
                'value' => $post->ID,
                'label' => $post->post_title
            );
            endwhile;
        endif;
        wp_reset_postdata();
        return $list;
    }
}
if ( ! function_exists( 's7upf_catalog_ordering' ) ) {
    function s7upf_catalog_ordering($query,$set_orderby = '') {        
        $orderby                 = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
        if(!empty($set_orderby)) $orderby = $set_orderby;
        $show_default_orderby    = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
        $catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array(
            'menu_order' => __( 'Default sorting', 'lucky' ),
            'popularity' => __( 'Sort by popularity', 'lucky' ),
            'rating'     => __( 'Sort by average rating', 'lucky' ),
            'date'       => __( 'Sort by newness', 'lucky' ),
            'price'      => __( 'Sort by price: low to high', 'lucky' ),
            'price-desc' => __( 'Sort by price: high to low', 'lucky' )
        ) );

        if ( ! $show_default_orderby ) {
            unset( $catalog_orderby_options['menu_order'] );
        }

        if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
            unset( $catalog_orderby_options['rating'] );
        }

        wc_get_template( 'loop/orderby.php', array( 'catalog_orderby_options' => $catalog_orderby_options, 'orderby' => $orderby, 'show_default_orderby' => $show_default_orderby ) );
    }
}
if(!function_exists('s7upf_get_icon_params')){
    function s7upf_get_icon_params($key = '',$value = ''){
        $params = array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Icon library', 'lucky' ),
                'value' => array(
                    esc_html__( 'Font Awesome', 'lucky' ) => 'fontawesome',
                    esc_html__( 'Open Iconic', 'lucky' ) => 'openiconic',
                    esc_html__( 'Typicons', 'lucky' ) => 'typicons',
                    esc_html__( 'Entypo', 'lucky' ) => 'entypo',
                    esc_html__( 'Linecons', 'lucky' ) => 'linecons',
                    esc_html__( 'Mono Social', 'lucky' ) => 'monosocial',
                ),
                'param_name' => 'type',
                'description' => esc_html__( 'Select icon library.', 'lucky' ),
                'dependency' => array(
                    'element' => $key,
                    'value' => $value,
                    )
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'lucky' ),
                'param_name' => 'icon_fontawesome',
                'value' => 'fa fa-adjust', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'fontawesome',
                ),
                'description' => esc_html__( 'Select icon from library.', 'lucky' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'lucky' ),
                'param_name' => 'icon_openiconic',
                'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'openiconic',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'openiconic',
                ),
                'description' => esc_html__( 'Select icon from library.', 'lucky' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'lucky' ),
                'param_name' => 'icon_typicons',
                'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'typicons',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'typicons',
                ),
                'description' => esc_html__( 'Select icon from library.', 'lucky' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'lucky' ),
                'param_name' => 'icon_entypo',
                'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'entypo',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'entypo',
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'lucky' ),
                'param_name' => 'icon_linecons',
                'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'linecons',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'linecons',
                ),
                'description' => esc_html__( 'Select icon from library.', 'lucky' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__( 'Icon', 'lucky' ),
                'param_name' => 'icon_monosocial',
                'value' => 'vc-mono vc-mono-fivehundredpx', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'monosocial',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'monosocial',
                ),
                'description' => esc_html__( 'Select icon from library.', 'lucky' ),
            ),
        );
        return $params;
    }
}

/***************************************END Theme Function***************************************/
