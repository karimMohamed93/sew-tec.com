<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 18/08/15
 * Time: 10:00 AM
 */
// Start 15/10/2016
if(!function_exists('s7upf_vc_menu'))
{
    function s7upf_vc_menu($attr,$content = false)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'     => 'main-nav1',
            'menu'      => '',
        ),$attr));
        if(!empty($menu)){
            $html .=    '<nav class="main-nav '.esc_attr($style).'">
                            <a href="#" class="btn-menu-hover"><i class="fa fa-bars" aria-hidden="true"></i></a>';
                            ob_start();
                            wp_nav_menu( array(
                                'menu' => $menu,
                                'container'=>false,
                                'walker'=>new S7upf_Walker_Nav_Menu(),
                            ));
            $html .=        @ob_get_clean();
            $html .=        '<a href="#" class="toggle-mobile-menu"><span></span></a>';
            $html .=    '</nav>';
        }
        else{
            $html .=    '<nav class="main-nav '.esc_attr($style).'">
                            <a href="#" class="btn-menu-hover"><i class="fa fa-bars" aria-hidden="true"></i></a>';
                            ob_start();
                            wp_nav_menu( array(
                                'theme_location' => 'primary',
                                'container'=>false,
                                'walker'=>new S7upf_Walker_Nav_Menu(),
                            ));
            $html .=        @ob_get_clean();
            $html .=        '<a href="#" class="toggle-mobile-menu"><span></span></a>';
            $html .=    '</nav>';
        }        
        return $html;
    }
}

stp_reg_shortcode('s7upf_menu','s7upf_vc_menu');

vc_map( array(
    "name"      => esc_html__("SV Menu", 'lucky'),
    "base"      => "s7upf_menu",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type"              => "dropdown",
            "heading"           => esc_html__("Style",'lucky'),
            "param_name"        => "style",
            "value"             => array(
                                    esc_html__("Default",'lucky')   => 'main-nav1',
                                    esc_html__("Home 3",'lucky')   => 'main-nav3',
                                    esc_html__("Home 5",'lucky')   => 'main-nav5',
                                    esc_html__("Home 8",'lucky')   => 'main-nav8',
                                    esc_html__("Home 9",'lucky')   => 'main-nav3 main-nav9',
                                    esc_html__("Home 12",'lucky')   => 'main-nav3 main-nav1',
                                    esc_html__("Home 14",'lucky')   => 'main-nav14',
                                )
        ),
        array(
            'type'              => 'dropdown',
            'holder'            => 'div',
            'heading'           => esc_html__( 'Menu name', 'lucky' ),
            'param_name'        => 'menu',
            'value'             => s7upf_list_menu_name(),
            'description'       => esc_html__( 'Select Menu name to display', 'lucky' )
        ),
    )
));