<?php
/**
 * @version    1.0
 * @package    Aqualoa
 * @author     7up Team <7uptheme.com>
 * @copyright  Copyright (C) 2015 7uptheme. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.7uptheme.com
 */
if(!function_exists('s7upf_vc_social'))
{
    function s7upf_vc_social($attr, $content = false)
    {
        $html = '';
        extract(shortcode_atts(array(
            'list'          => '',
            'align'         => 'text-left',
        ),$attr));
		parse_str( urldecode( $list ), $data);
        $html .=    '<div class="social-footer '.$align.'">';
                        if(is_array($data))
                        {
                            foreach ($data as $key => $value) {
                                $url = '#';
                                if(isset($value['url'])) $url = $value['url'];
                                $html .= '<a href="'.esc_url($url).'"><i class="fa '.$value['social'].'"></i></a>';
                            }
                        }
        $html .=    '</div>';   
		return  $html;
    }
}

stp_reg_shortcode('sv_social','s7upf_vc_social');


vc_map( array(
    "name"      => esc_html__("SV Social", 'lucky'),
    "base"      => "sv_social",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Align', 'lucky' ),
			'value' => array(
				esc_html__( 'Align Left', 'lucky' ) => 'text-left',
				esc_html__( 'Align Center', 'lucky' ) => 'text-center',
				esc_html__( 'Align Right', 'lucky' ) => 'text-right',
			),
			'param_name' => 'align',
			'description' => esc_html__( 'Select social layout', 'lucky' ),
		),
		array(
            "type" => "add_social",
            "heading" => esc_html__("Add Social List",'lucky'),
            "param_name" => "list",
        )
    )
));