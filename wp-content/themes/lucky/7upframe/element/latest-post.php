<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 15/12/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_lastest_post'))
{
    function s7upf_vc_lastest_post($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'     => '',
            'title'     => '',
            'cats'      => '',
            'number'    => '8',
            'order'     => 'DESC',
            'link'         => '',
        ),$attr));
        $args = array(
            'post_type'         => 'post',
            'posts_per_page'    => $number,
            'orderby'           => 'date',
            'order'             => $order,
        );
        if(!empty($cats)) {
            $custom_list = explode(",",$cats);
            $args['tax_query'][]=array(
                'taxonomy'=>'category',
                'field'=>'slug',
                'terms'=> $custom_list
            );
        }
        $query = new WP_Query($args);
        $count = 1;
        $count_query = $query->post_count;
        switch ($style) {
            case 'home-14':
                $html .=    '<div class="contain-slider14">';
                if(!empty($title)) $html .=    '<div class="title-box1 style2">
                                                    <h2 class="title30"><span>'.esc_html($title).'</span></h2>
                                                </div>';
                $html .=        '<div class="latest-newslider14">
                                    <div class="wrap-item arrow-style14 smart-slider" data-item="3" data-speed="" data-itemres="0:1,568:2,1024:3" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=        '<div class="item-latest-news style2">
                                            <div class="post-thumb">
                                                <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(370,370)).'</a>
                                            </div>
                                            <div class="post-info">
                                                <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                <ul class="post-comment-date list-none">
                                                    <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                    <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                </ul>
                                                <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).' [...]</p>
                                            </div>
                                        </div>';
                    }
                }
                $html .=            '</div>';
                if(!empty($link)) $html .=    '<a href="'.esc_url($link).'" class="btn-product-loadmore border radius">'.esc_html__("View All","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'home-13':
                $html .=    '<div class="latest-news13">';
                if(!empty($title)) $html .=    '<h2 class="title16 title-box12"><span>'.esc_html($title).'</span></h2>';
                $html .=        '<div class="latest-newslider">
                                    <div class="wrap-item smart-slider" data-item="4" data-speed="" data-itemres="0:1,600:2,980:3,1200:4" data-prev="" data-next="" data-pagination="" data-navigation="">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=        '<div class="item-latest-news style2">
                                            <div class="post-thumb">
                                                <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(270,500)).'</a>
                                            </div>
                                            <div class="post-info">
                                                <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                <ul class="post-comment-date list-none">
                                                    <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                    <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                </ul>
                                                <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).' [...]</p>
                                                <a href="'.esc_url(get_the_permalink()).'" class="btn-rect readmore radius text-uppercase">'.esc_html__("Read more","lucky").'</a>
                                            </div>
                                        </div>';
                    }
                }
                $html .=            '</div>';
                if(!empty($link)) $html .=    '<a href="'.esc_url($link).'" class="btn-product-loadmore border radius">'.esc_html__("View All","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'home-12':
                $html .=    '<div class="product-right-box12">';
                if(!empty($title)) $html .=    '<h2 class="title16 title-box12"><span>'.esc_html($title).'</span></h2>';
                $html .=        '<div class="product-slider-box12 blog-slider12">
                                    <div class="wrap-item long-arrow smart-slider" data-item="2" data-speed="" data-itemres="0:1,480:2" data-prev="" data-next="" data-pagination="" data-navigation="true">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        if($count % 2 == 1) $html .= '<div class="item-blog12">';
                        $html .=        '<div class="item-latest-news">
                                            <div class="post-thumb">
                                                <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(370,220)).'</a>
                                                <a href="'.esc_url(get_the_post_thumbnail_url(get_the_ID(),'full')).'" class="post-thumb-zoom"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            </div>
                                            <div class="post-info">
                                                <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                <ul class="post-comment-date list-none">
                                                    <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                    <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                </ul>
                                            </div>
                                        </div>';
                        if($count % 2 == 0 || $count == $count_query) $html .=  '</div>';
                        $count++;
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'home-11':
                $html .=    '<div class="latest-news11">';
                if(!empty($title)) $html .=    '<h2 class="title30"><span>'.esc_html($title).'</span></h2>';
                $html .=        '<div class="latest-news1">
                                    <div class="latest-newslider">
                                        <div class="wrap-item long-arrow smart-slider" data-item="2" data-speed="" data-itemres="0:1,767:2" data-prev="" data-next="" data-pagination="" data-navigation="true">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=            '<div class="item-latest-news style2">
                                                <div class="post-thumb">
                                                    <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(570,570)).'</a>
                                                </div>
                                                <div class="post-info">
                                                    <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                    <ul class="post-comment-date list-none">
                                                        <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                        <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                    </ul>
                                                    <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).' [...]</p>
                                                    <a href="'.esc_url(get_the_permalink()).'" class="btn-rect readmore radius text-uppercase">'.esc_html__("Read more","lucky").'</a>
                                                </div>
                                            </div>';
                    }
                }
                $html .=                '</div>
                                    </div>
                                </div>
                            </div>';
                break;

            case 'home-9':
                $html .=        '<div class="ourblog-slider9">
                                    <div class="wrap-item long-arrow smart-slider" data-item="1" data-speed="" data-itemres="" data-prev="" data-next="" data-pagination="" data-navigation="true">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=        '<div class="item-blog9">
                                            <div class="post-thumb">
                                                <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(640,400)).'</a>
                                            </div>
                                            <div class="post-info">
                                                <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                <ul class="post-comment-date list-none">
                                                    <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                    <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                </ul>
                                                <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).'</p>
                                                <a href="'.esc_url(get_the_permalink()).'" class="btn-rect radius">'.esc_html__("Read more","lucky").'</a>
                                            </div>
                                        </div>';
                    }
                }
                $html .=        '</div>
                            </div>';
                break;

            case 'home-7':
                $html .=    '<div class="latest-news7">';
                if(!empty($title)){
                $html .=        '<div class="title-box1 text-center">
                                    <h2 class="title30"><span>'.esc_html($title).'</span></h2>
                                </div>';
                }
                $html .=        '<div class="row">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        if($count % 4 == 1){
                            $html .=    '<div class="col-md-6 hidden-sm col-xs-12">
                                            <div class="item-latest-news style2">
                                                <div class="post-thumb">
                                                    <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(570,570)).'</a>
                                                </div>
                                                <div class="post-info">
                                                    <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                    <ul class="post-comment-date list-none">
                                                        <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                        <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                    </ul>
                                                    <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).' [...]</p>
                                                    <a href="'.esc_url(get_the_permalink()).'" class="btn-rect readmore radius text-uppercase">'.esc_html__("Read more","lucky").'</a>
                                                </div>
                                            </div>
                                        </div>';
                        }
                        if($count % 4 == 2 || $count % 4 == 3){
                            if($count % 4 == 2) $html .=    '<div class="col-md-3 col-sm-6 col-xs-6">';
                            $html .=    '<div class="item-latest-news style2">
                                            <div class="post-thumb">
                                                <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(270,270)).'</a>
                                            </div>
                                            <div class="post-info none-desc">
                                                <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                <ul class="post-comment-date list-none">
                                                    <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                    <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                </ul>
                                                <a href="'.esc_url(get_the_permalink()).'" class="btn-rect readmore radius text-uppercase">'.esc_html__("Read more","lucky").'</a>
                                            </div>
                                        </div>';
                            if($count % 4 == 3 || $count == $count_query) $html .=    '</div>';
                        }
                        if($count % 4 == 0){
                            $html .=    '<div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="item-latest-news style2">
                                                <div class="post-thumb">
                                                    <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(270,570)).'</a>
                                                </div>
                                                <div class="post-info">
                                                    <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                    <ul class="post-comment-date list-none">
                                                        <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                        <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                                    </ul>
                                                    <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).' [...]</p>
                                                    <a href="'.esc_url(get_the_permalink()).'" class="btn-rect readmore radius text-uppercase">'.esc_html__("Read more","lucky").'</a>
                                                </div>
                                            </div>
                                        </div>';
                        }
                        $count++;
                    }
                }
                $html .=        '</div>';
                if(!empty($link)) $html .=    '<a href="'.esc_url($link).'" class="btn-product-loadmore border radius">'.esc_html__("View All","lucky").'</a>';
                $html .=    '</div>';
                break;

            case 'home-6':
                $html .=    '<div class="latest-news6">';
                if(!empty($title)){
                $html .=        '<div class="title-box1 text-center">
                                    <h2 class="title30"><span>'.esc_html($title).'</span></h2>
                                </div>';
                }
                $html .=        '<div class="latest-news1">
                                    <div class="latest-newslider">
                                        <div class="wrap-item smart-slider" data-item="3" data-speed="" data-itemres="0:1,568:1,667:2,1024:3" data-prev="" data-next="" data-pagination="" data-navigation="">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=    '<div class="item-latest-news style2">
                                        <div class="post-thumb">
                                            <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(370,370)).'</a>
                                        </div>
                                        <div class="post-info">
                                            <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            <ul class="post-comment-date list-none">
                                                <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                            </ul>
                                            <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).' [...]</p>
                                        </div>
                                    </div>';
                    }
                }
                $html .=                '</div>';
                if(!empty($link)) $html .=    '<a href="'.esc_url($link).'" class="btn-product-loadmore border radius">'.esc_html__("View All","lucky").'</a>';
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'home-3':
                $html .=    '<div class="latest-post3">';
                $html .=        '<h2>'.esc_html($title).'</h2>';
                $html .=        '<div class="list-latest-post3">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=    '<div class="item-latest-post3">
                                        <div class="latest-post-thumb3">
                                            <div class="post-thumb">
                                                <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(210,140)).'</a>
                                                <a href="'.esc_url(get_the_post_thumbnail_url(get_the_ID(),'full')).'" class="post-thumb-zoom"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <div class="latest-post-info3">
                                            <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            <ul class="post-comment-date list-none">
                                                <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                            </ul>
                                            <p class="desc">'.s7upf_substr(get_the_excerpt(),0,70).'</p>
                                        </div>
                                    </div>';
                    }
                }

                $html .=        '</div>
                            </div>';
                break;
            
            default:
                $html .=    '<div class="latest-news1">
                                <div class="latest-newslider">
                                    <div class="wrap-item smart-slider" data-item="3" data-speed="" data-itemres="" data-prev="" data-next="" data-pagination="" data-navigation="">';
                if($query->have_posts()) {
                    while($query->have_posts()) {
                        $query->the_post();
                        $html .=    '<div class="item-latest-news">
                                        <div class="post-thumb">
                                            <a class="post-thumb-link" href="'.esc_url(get_the_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),array(370,220)).'</a>
                                            <a href="'.esc_url(get_the_post_thumbnail_url(get_the_ID(),'full')).'" class="post-thumb-zoom"><i class="fa fa-search" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="post-info">
                                            <h3 class="post-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            <ul class="post-comment-date list-none">
                                                <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><span>'.get_the_date('M d Y').'</span></li>
                                                <li><i class="fa fa-comment-o" aria-hidden="true"></i><span>'.get_comments_number().' '.esc_html__("Comments","lucky").'</span></li>
                                            </ul>
                                            <p class="desc">'.s7upf_substr(get_the_excerpt(),0,80).'</p>
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>';
                if(!empty($link)) $html .=    '<a href="'.esc_url($link).'" class="btn-product-loadmore border radius">'.esc_html__("See more","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;
        }
       
        wp_reset_postdata();
        return $html;
    }
}

stp_reg_shortcode('sv_lastest_post','s7upf_vc_lastest_post');

vc_map( array(
    "name"      => esc_html__("SV Latest Post", 'lucky'),
    "base"      => "sv_lastest_post",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "holder" => "div",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__("Default",'lucky')   => '',
                esc_html__("Home 3",'lucky')   => 'home-3',
                esc_html__("Home 6",'lucky')   => 'home-6',
                esc_html__("Home 7",'lucky')   => 'home-7',
                esc_html__("Home 9",'lucky')   => 'home-9',
                esc_html__("Home 11",'lucky')   => 'home-11',
                esc_html__("Home 12",'lucky')   => 'home-12',
                esc_html__("Home 13",'lucky')   => 'home-13',
                esc_html__("Home 14",'lucky')   => 'home-14',
                )
        ),
        array(
            'type'        => 'textfield',
            'holder'      => 'div',
            'heading'     => esc_html__( 'Title', 'lucky' ),
            'param_name'  => 'title',
            'dependency'    => array(
                'element'   => 'style',
                'value'     => array('home-3','home-6','home-7','home-11','home-12','home-14'),
                )
        ),
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Number post', 'lucky' ),
            'param_name'  => 'number',
            'description' => esc_html__( 'Number posts are display. Default is 8.', 'lucky' ),
        ),
        array(
            'holder'     => 'div',
            'heading'     => esc_html__( 'Categories', 'lucky' ),
            'type'        => 'checkbox',
            'param_name'  => 'cats',
            'value'       => s7upf_list_taxonomy('category',false)
        ),
        array(
            'heading'     => esc_html__( 'Order', 'lucky' ),
            'type'        => 'dropdown',
            'param_name'  => 'order',
            'value' => array(                   
                esc_html__('Desc','lucky')  => 'DESC',
                esc_html__('Asc','lucky')  => 'ASC',
            ),
            'description' => esc_html__( 'Select Order Type ', 'lucky' ),
        ),        
        array(
            "type" => "textfield",
            "heading" => esc_html__("Link view more",'lucky'),
            "param_name" => "link",
        ),
    )
));