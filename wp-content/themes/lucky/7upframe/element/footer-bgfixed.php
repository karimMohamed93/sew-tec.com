<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_footer_bg'))
{
    function s7upf_vc_footer_bg($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'image'      => '',
        ),$attr));
        if(!empty($image)) $image = S7upf_Assets::build_css('background-image: url('.wp_get_attachment_url($image,'full').');');
        $html .=    '<div class="footer-bg-data" data-bg="footer-parallax '.esc_attr($image).'"></div>';
        
        return $html;
    }
}

stp_reg_shortcode('s7upf_footer_bg','s7upf_vc_footer_bg');

vc_map( array(
    "name"      => esc_html__("SV Footer Background Fixed", 'lucky'),
    "base"      => "s7upf_footer_bg",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Image Background",'lucky'),
            "param_name" => "image",
        )
    )
));