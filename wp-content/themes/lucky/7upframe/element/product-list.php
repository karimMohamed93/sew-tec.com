<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 05/09/15
 * Time: 10:00 AM
 */
if(class_exists("woocommerce")){
if(!function_exists('sv_vc_product_list'))
{
    function sv_vc_product_list($attr, $content = false)
    {
        $html = $view_html = '';
        extract(shortcode_atts(array(
            'style'         => 'home-3',
            'title'         => '',
            'number'        => '8',
            'cats'          => '',
            'order_by'      => 'date',
            'order'         => 'DESC',
            'product_type'  => '',
            'item'          => '',
            'item_res'      => '',
            'speed'         => '',
            'size'          => '',
            'time'          => '',
            'time2'         => '',
            'animation'     => '',
            'view_link'     => '',
        ),$attr));
        $animation_class = '';
        if(!empty($animation)) {
            $animation_class = ' wow '.$animation;
        }
        $custom_list = array();
        $args = array(
            'post_type'         => 'product',
            'posts_per_page'    => $number,
            'orderby'           => $order_by,
            'order'             => $order,
            'paged'             => 1,
            );
        if($product_type == 'trendding'){
            $args['meta_query'][] = array(
                    'key'     => 'trending_product',
                    'value'   => 'on',
                    'compare' => '=',
                );
        }
        if($product_type == 'toprate'){
            $args['meta_key'] = '_wc_average_rating';
            $args['orderby'] = 'meta_value_num';
            $args['meta_query'] = WC()->query->get_meta_query();
            $args['tax_query'][] = WC()->query->get_tax_query();
        }
        if($product_type == 'mostview'){
            $args['meta_key'] = 'post_views';
            $args['orderby'] = 'meta_value_num';
        }
        if($product_type == 'bestsell'){
            $args['meta_key'] = 'total_sales';
            $args['orderby'] = 'meta_value_num';
        }
        if($product_type=='onsale'){
            $args['meta_query']['relation']= 'OR';
            $args['meta_query'][]=array(
                'key'   => '_sale_price',
                'value' => 0,
                'compare' => '>',                
                'type'          => 'numeric'
            );
            $args['meta_query'][]=array(
                'key'   => '_min_variation_sale_price',
                'value' => 0,
                'compare' => '>',                
                'type'          => 'numeric'
            );
        }
        if($product_type == 'featured'){
            $args['meta_key'] = '_featured';
            $args['meta_value'] = 'yes';
        }
        if(!empty($cats)) {
            $custom_list = explode(",",$cats);
            $args['tax_query'][]=array(
                'taxonomy'=>'product_cat',
                'field'=>'slug',
                'terms'=> $custom_list
            );
        }
        $product_query = new WP_Query($args);
        $count = 1;
        $count_query = $product_query->post_count;
        if(!empty($size)) $size = explode('x', $size);
        switch ($style) {
            case 'tab-home1':
                if(!empty($cats)){
                    if(empty($item) && empty($item_res)) $item_res = '0:1,480:2,767:3,980:4';
                    if(empty($item)) $item = 4;
                    if(empty($size)) $size = array(270,336);
                    $pre = rand(1,100);
                    $tabs = explode(",",$cats);
                    $tab_html = $tab_content = '';
                    foreach ($tabs as $key => $tab) {
                        $term = get_term_by( 'slug',$tab, 'product_cat' );
                        if(!empty($term) && is_object($term)){
                            if($key == 0) $active = 'active';
                            else $active = '';
                            $cat_thumb_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
                            $tab_html .=    '<li class="'.$active.'"><a href="#'.$pre.$term->slug.'" data-toggle="tab">'.$term->name.'</a></li>';
                            $tab_content .=    '<div class="tab-pane '.$active.'" id="'.$pre.$term->slug.'">
                                                    <div class="product-slider5">
                                                        <div class="wrap-item smart-slider arrow-style3" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                            unset($args['tax_query']);
                            $args['tax_query'][]=array(
                                'taxonomy'=>'product_cat',
                                'field'=>'slug',
                                'terms'=> $tab
                            );
                            $product_query = new WP_Query($args);
                            $count = 1;
                            $count_query = $product_query->post_count;
                            if($product_query->have_posts()) {
                                while($product_query->have_posts()) {
                                    $product_query->the_post();
                                    global $product;
                                    if($count % 2 == 1) $tab_content .= '<div class="item">';
                                    $tab_content .=     '<div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                                '.s7upf_product_link().'
                                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                '.s7upf_get_rating_html().'
                                                                '.s7upf_get_saleoff_html().'
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                '.s7upf_get_price_html().'
                                                            </div>
                                                        </div>';
                                    if($count % 2 == 0 || $count == $count_query) $tab_content .= '</div>';
                                    $count++;
                                }
                            }
                            $tab_content .=             '</div>
                                                    </div>
                                                </div>';
                        }
                    }
                    $html .=    '<div class="product-box5 '.esc_attr($style).'">
                                    <div class="title-box5">';
                    if(!empty($title)) $html .=    '<h2 class="title18">'.esc_html($title).'</h2>';
                    $html .=            '<ul class="list-none">
                                            '.$tab_html.'
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        '.$tab_content.'
                                    </div>
                                </div>';
                }
                break;

            case 'list-home-14':
                if(empty($item) && empty($item_res)) $item_res = '0:1,480:2,767:3,1200:4';
                if(empty($item)) $item = 4;
                if(empty($size)) $size = array(250,310);
                $html .=    '<div class="contain-slider14 product-list14 '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<div class="title-box1 style2">
                                                    <h2 class="title30"><span>'.esc_html($title).'</span></h2>
                                                </div>';
                $html .=        '<div class="product-slider14">';
                $html .=            '<div class="wrap-item arrow-style14 smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html .=    '<div class="item-product'.esc_attr($animation_class).'">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            '.s7upf_get_rating_html().'
                                            '.s7upf_get_saleoff_html().'
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'side-home-12':
                if(empty($item) && empty($item_res)) $item_res = '0:1,640:2,1024:1';
                if(empty($item)) $item = 1;
                if(empty($size)) $size = array(70,87);
                $html .=    '<div class="box-product-type shadow-box '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<h2 class="title16">'.esc_html($title).'</h2>';
                $html .=        '<div class="product-type-slider">';
                $html .=            '<div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="true" data-navigation="">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html_wl = '';
                        if(class_exists('YITH_WCWL_Init')) $html_wl = '<a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'add_to_wishlist', get_the_ID() ))).'" class="add_to_wishlist wishlist-link  pull-right" rel="nofollow" data-product-id="'.esc_attr(get_the_ID()).'" data-product-title="'.esc_attr(get_the_title()).'"><i class="fa fa-heart-o" aria-hidden="true"></i></a>';
                        if($count % 4 == 1) $html .=    '<div class="item">';
                        $html .=        '<div class="item-product-type table'.esc_attr($animation_class).'">
                                            <div class="product-thumb">
                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            </div>
                                            <div class="product-info">
                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                '.s7upf_get_price_html().'
                                                <div class="clearfix">
                                                    '.s7upf_get_rating_html(true,'rate-counter pull-left').'
                                                    '.$html_wl.'
                                                </div>
                                            </div>
                                        </div>';
                        if($count % 4 == 0 || $count == $count_query) $html .=    '</div>';
                        $count++;
                    }
                }
                $html .=            '</div>';
                if(!empty($view_link)) $html .=    '<a href="'.esc_url($view_link).'" class="btn-rect border radius text-uppercase">'.esc_html__("view all","lucky").'</a>';
                $html .=        '</div>
                            </div>';
                break;

            case 'tab-home12':
                if(!empty($cats)){
                    if(empty($item) && empty($item_res)) $item_res = '0:1,480:2,767:3,980:4';
                    if(empty($item)) $item = 4;
                    if(empty($size)) $size = array(270,336);
                    $pre = rand(1,100);
                    $tabs = explode(",",$cats);
                    $tab_html = $tab_content = '';
                    foreach ($tabs as $key => $tab) {
                        $term = get_term_by( 'slug',$tab, 'product_cat' );
                        if(!empty($term) && is_object($term)){
                            if($key == 0) $active = 'active';
                            else $active = '';
                            $cat_thumb_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
                            $tab_html .=    '<li class="'.$active.'"><a href="#'.$pre.$term->slug.'" data-toggle="tab">'.$term->name.'</a></li>';
                            $tab_content .=    '<div class="tab-pane '.$active.'" id="'.$pre.$term->slug.'">
                                                    <div class="product-tab-slider12">
                                                        <div class="wrap-item smart-slider long-arrow" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                            unset($args['tax_query']);
                            $args['tax_query'][]=array(
                                'taxonomy'=>'product_cat',
                                'field'=>'slug',
                                'terms'=> $tab
                            );
                            $product_query = new WP_Query($args);
                            $count = 1;
                            $count_query = $product_query->post_count;
                            if($product_query->have_posts()) {
                                while($product_query->have_posts()) {
                                    $product_query->the_post();
                                    global $product;
                                    $tab_content .= '<div class="item-product'.esc_attr($animation_class).'">
                                                        <div class="product-thumb">
                                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                            '.s7upf_product_link().'
                                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                            '.s7upf_get_rating_html().'
                                                            '.s7upf_get_saleoff_html().'
                                                        </div>
                                                        <div class="product-info">
                                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                            '.s7upf_get_price_html().'
                                                        </div>
                                                    </div>';
                                }
                            }
                            $tab_content .=             '</div>
                                                    </div>
                                                </div>';
                        }
                    }
                    $html .=    '<div class="product-tab12 '.esc_attr($style).'">';
                    if(!empty($title)) $html .=    '<h2 class="title16">'.esc_html($title).'</h2>';
                    $html .=        '<div class="title-tab1">
                                        <ul class="list-none">
                                            '.$tab_html.'
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        '.$tab_content.'
                                    </div>
                                </div>';
                }
                break;

            case 'list-home-12':
                if(empty($item) && empty($item_res)) $item_res = '0:1,480:2,767:3';
                if(empty($item)) $item = 3;
                if(empty($size)) $size = array(310,385);
                $html .=    '<div class="product-right-box12 '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<h2 class="title16 title-box12">'.esc_html($title).'</h2>';
                $html .=        '<div class="product-slider-box12">';
                $html .=            '<div class="wrap-item long-arrow smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html .=    '<div class="item-product'.esc_attr($animation_class).'">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            '.s7upf_get_rating_html().'
                                            '.s7upf_get_saleoff_html().'
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'deal-home-12':
                if(empty($item) && empty($item_res)) $item_res = '0:1,560:2,980:3,1024:1';
                if(empty($item)) $item = 1;
                if(empty($size)) $size = array(310,385);
                $html .=    '<div class="box-product-type shadow-box '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<h2 class="title16">'.esc_html($title).'</h2>';
                $html .=        '<div class="hotdeal-slider12">';
                $html .=            '<div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="true" data-navigation="">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html .=    '<div class="item-product none-shadow item-product-deal12'.esc_attr($animation_class).'">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            '.s7upf_get_saleoff_html().'
                                        </div>';
                        if(!empty($time)) $html .=    '<div class="hotdeal-countdown clock-countdown" data-date="'.esc_attr($time).'"></div>';
                        $html .=        '<div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                            '.s7upf_product_link().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'side-home-11':
                if(empty($item) && empty($item_res)) $item_res = '0:1';
                if(empty($item)) $item = 1;
                if(empty($size)) $size = array(70,87);
                $html .=    '<div class="box-product-type border '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<h2 class="title16">'.esc_html($title).'</h2>';
                $html .=        '<div class="product-type-slider">';
                $html .=            '<div class="wrap-item long-arrow smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html_wl = '';
                        if(class_exists('YITH_WCWL_Init')) $html_wl = '<a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'add_to_wishlist', get_the_ID() ))).'" class="add_to_wishlist wishlist-link  pull-right" rel="nofollow" data-product-id="'.esc_attr(get_the_ID()).'" data-product-title="'.esc_attr(get_the_title()).'"><i class="fa fa-heart-o" aria-hidden="true"></i></a>';
                        if($count % 3 == 1) $html .=    '<div class="item">';
                        $html .=        '<div class="item-product-type table'.esc_attr($animation_class).'">
                                            <div class="product-thumb">
                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            </div>
                                            <div class="product-info">
                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                '.s7upf_get_price_html().'
                                                <div class="clearfix">
                                                    '.s7upf_get_rating_html(true,'rate-counter pull-left').'
                                                    '.$html_wl.'
                                                </div>
                                            </div>
                                        </div>';
                        if($count % 3 == 0 || $count == $count_query) $html .=    '</div>';
                        $count++;
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'deal-home-11':
                if(empty($item) && empty($item_res)) $item_res = '';
                if(empty($item)) $item = 1;
                if(empty($size)) $size = array(570,470);
                $curren_time = getdate();
                $time2 = explode(':', $time2);
                $hours = $min = 0;
                if(isset($time2[0])) $hours = (int)$time2[0];
                if(isset($time2[1])) $min = (int)$time2[1];
                $data_h = $hours - $curren_time['hours'];
                $data_m = $min - $curren_time['minutes'];
                $data_time = $data_h*3600+$data_m*60+60-$curren_time['seconds'];
                $html .=    '<div class="deal-product-box11 '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<h2 class="title30">'.esc_html($title).'</h2>';
                $html .=        '<div class="deal-slider11">';
                $html .=            '<div class="wrap-item long-arrow smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        global $product;
                        $thumb_id = array(get_post_thumbnail_id());
                        $attachment_ids = $product->get_gallery_image_ids();
                        $attachment_ids = array_merge($thumb_id,$attachment_ids);
                        $ul_block = $bx_block = '';$i = 0;
                        $available_data = array();
                        if( $product->is_type( 'variable' ) ) $available_data = $product->get_available_variations();
                        foreach ( $attachment_ids as $attachment_id ) {
                            $image_link = wp_get_attachment_url( $attachment_id );
                            if ( ! $image_link )
                                continue;
                            if($i == 0) $active = 'active';
                            else $active = '';
                            $image_pager       = wp_get_attachment_image( $attachment_id, $size);
                            $ul_block .= '<li><a href="#" class="'.esc_attr($active).'">'.$image_pager.'</a></li>';
                            $i++;
                        }
                        if(!empty($available_data)){
                            foreach ($available_data as $available) {
                                if(!empty($available['image_src'])){
                                    $image2 = wp_get_attachment_image( $available['image_id'], $size, 0, $attr = array(
                                        'title' => $image_title,
                                        'alt'   => $image_title
                                        ) );
                                    $ul_block .= '<li><a href="#">'.$image2.'</a></li>';
                                }
                            }
                        }
                        $total_sales = get_post_meta(get_the_ID(),'total_sales',true);
                        $total_stock = $product->get_stock_quantity();
                        if(empty($total_stock)) $total_stock = 50;
                        $percent = $total_sales/$total_stock*100;
                        $html .=        '<div class="item-deal11">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="deal-gallery '.esc_attr($animation_class).'">
                                                        <div class="product-thumb">
                                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="deal-info11 '.esc_attr($animation_class).'">
                                                        <h2 class="title30">'.get_the_title().'</h2>
                                                        '.s7upf_get_price_html().'
                                                        '.s7upf_get_rating_html(true,'rate-counter').'
                                                        <div class="deal-control">
                                                            <ul class="list-none">
                                                                '.$ul_block.'
                                                            </ul>
                                                        </div>
                                                        <div class="store-process">
                                                            <span class="product-instock product-sold">'.esc_html__("Already Sold:","lucky").'  <b>'.$total_sales.'</b></span>
                                                            <div class="percent-store" style="width:'.esc_attr($percent).'%"></div>
                                                            <span class="product-instock">'.esc_html__("Available:","lucky").'  <b>'.$total_stock.'</b></span>
                                                        </div>';
                        if(!empty($time2)){
                            $html .=                    '<div class="deal-master11">
                                                            <span>'.esc_html__("Ends in:","lucky").'</span>
                                                            <div class="countdown-master" data-time="'.esc_attr($data_time).'"></div>
                                                        </div>';
                        }
                        $html .=                        ' '.s7upf_product_link('deal-home11').'
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
                    }
                }
                $html .=                '</div>
                                    </div>
                                </div>
                            </div>';
                break;

            case 'home-11':
                if(empty($item) && empty($item_res)) $item_res = '0:1,480:2,767:3,980:4';
                if(empty($item)) $item = 4;
                if(empty($size)) $size = array(270,336);
                $html .=    '<div class="new-product11 '.esc_attr($style).'">';
                if(!empty($title)) $html .=    '<h2 class="title30">'.esc_html($title).'</h2>';
                $html .=        '<div class="newpro-slider11">';
                $html .=            '<div class="wrap-item long-arrow smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html .=        '<div class="item-product none-shadow '.esc_attr($animation_class).'">
                                            <div class="product-thumb">
                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                '.s7upf_product_link().'
                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                '.s7upf_get_rating_html().'
                                                '.s7upf_get_saleoff_html().'
                                            </div>
                                            <div class="product-info">';
                        $html .=                '<h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                '.s7upf_get_price_html().'
                                            </div>
                                        </div>';
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;

            case 'tab-home11':
                if(!empty($cats)){
                    if(empty($item) && empty($item_res)) $item_res = '0:1';
                    if(empty($item)) $item = 1;
                    if(empty($size)) $size = array(270,336);
                    $sizex = array(570,335);
                    $pre = rand(1,100);
                    $tabs = explode(",",$cats);
                    $tab_html = $tab_content = '';
                    foreach ($tabs as $key => $tab) {
                        $term = get_term_by( 'slug',$tab, 'product_cat' );
                        if(!empty($term) && is_object($term)){
                            if($key == 0) $active = 'active';
                            else $active = '';
                            $cat_thumb_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
                            $tab_html .=    '<li class="'.$active.'"><a href="#'.$pre.$term->slug.'" data-toggle="tab">'.wp_get_attachment_image($cat_thumb_id,array(15,27)).$term->name.'</a></li>';
                            $tab_content .=    '<div class="tab-pane '.$active.'" id="'.$pre.$term->slug.'">
                                                    <div class="product-slider11">
                                                        <div class="wrap-item smart-slider long-arrow" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                            unset($args['tax_query']);
                            $args['tax_query'][]=array(
                                'taxonomy'=>'product_cat',
                                'field'=>'slug',
                                'terms'=> $tab
                            );
                            $product_query = new WP_Query($args);
                            $count = 1;
                            $count_query = $product_query->post_count;
                            if($product_query->have_posts()) {
                                while($product_query->have_posts()) {
                                    $product_query->the_post();
                                    global $product;
                                    $large_item =   '<div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$sizex).'</a>
                                                                '.s7upf_product_link().'
                                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                '.s7upf_get_rating_html().'
                                                                '.s7upf_get_saleoff_html().'
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                '.s7upf_get_price_html().'
                                                            </div>
                                                        </div>
                                                    </div>';
                                    $small_item =   '<div class="col-md-3 col-sm-3 col-xs-6">
                                                        <div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                                '.s7upf_product_link().'
                                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                '.s7upf_get_rating_html().'
                                                                '.s7upf_get_saleoff_html().'
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                '.s7upf_get_price_html().'
                                                            </div>
                                                        </div>
                                                    </div>';
                                    if($count % 3 == 1) $tab_content .= '<div class="item"><div class="row">';
                                    if($count % 3 == 2) $tab_content .= $large_item;
                                    else $tab_content .= $small_item;
                                    if($count_query == $count || $count % 3 == 0) $tab_content .=    '</div></div>';
                                    $count++;
                                }
                            }
                            $tab_content .=             '</div>
                                                    </div>
                                                </div>';
                        }
                    }
                    $html .=    '<div class="product-bestsale11 '.esc_attr($style).'">';
                    if(!empty($title)) $html .=    '<h2 class="title30">'.esc_html($title).'</h2>';
                    $html .=        '<div class="nav-tabs-icon">
                                        <ul class="list-inline">
                                            '.$tab_html.'
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        '.$tab_content.'
                                    </div>
                                </div>';
                }
                break;

            case 'home-10':
                if(empty($item) && empty($item_res)) $item_res = '0:1,480:2,767:3,980:4,1200:5,1366:6';
                if(empty($item)) $item = 5;
                if(empty($size)) $size = array(270,336);
                if(!empty($time)) $item_class = 'item-product-deal10';
                else $item_class = '';
                $html .=    '<div class="product-slider10">';
                $html .=        '<div class="wrap-item long-arrow smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html .=    '<div class="item-product none-shadow '.esc_attr($item_class.$animation_class).'">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            '.s7upf_get_rating_html().'
                                            '.s7upf_get_saleoff_html().'
                                        </div>
                                        <div class="product-info">';
                        if(!empty($time)) $html .=    '<div class="hotdeal-countdown clock-countdown" data-date="'.esc_attr($time).'"></div>';
                        $html .=            '<h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_price_html().'
                                        </div>
                                    </div>';
                    }
                }
                $html .=        '</div>
                            </div>';
                break;

            case 'home-9':
                if(empty($size)) $size = array(370,459);                
                $html .=    '<div class="list-product9">';
                $count_block = 1;
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $is_featured = get_post_meta(get_the_ID(),'_featured',true);
                        $large_item =   '<div class="product-main banner-adv'.esc_attr($animation_class).'">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(520,647)).'</a>
                                            <div class="banner-info">';
                        if($is_featured == 'yes') $large_item .=   '<span>Featured</span>';
                        $large_item .=      '<h2 class="title30">'.get_the_title().'</h2>
                                                '.s7upf_get_price_html().'
                                                <p class="desc">'.get_the_excerpt().'</p>
                                                <a href="'.esc_url(get_the_permalink()).'" class="btn-rect radius">'.esc_html__("shop now","lucky").'</a>
                                            </div>
                                        </div>';
                        $small_item =   '<div class="item-product none-shadow'.esc_attr($animation_class).'">
                                            <div class="product-thumb">
                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                '.s7upf_product_link().'
                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                '.s7upf_get_rating_html().'
                                                '.s7upf_get_saleoff_html().'
                                            </div>
                                        </div>';
                        if($count_block % 2 == 1){
                            if($count % 6 == 1){
                                $html .=    '<div class="row">';
                                $html .=    '<div class="col-md-8 col-sm-8 col-xs-12">'.$large_item.'</div>';
                                if($count == $count_query) $html .= '</div>';
                            }
                            else{
                                if($count % 6 == 2) $html .= '<div class="col-md-4 col-sm-4 col-xs-12">
                                                                <div class="row">';
                                if($count % 6 == 2 || $count % 6 == 3) $html .= '<div class="col-md-12 col-sm-12 col-xs-6">';
                                if($count % 6 == 4) $html .='<div class="row list9-3item">';
                                if($count % 6 == 4 || $count % 6 == 5 || $count % 6 == 0) $html .='<div class="col-md-4 col-sm-4 col-xs-6">';
                                $html .= $small_item;
                                if($count % 6 == 2 || $count % 6 == 3) $html .= '</div>';
                                if($count % 6 == 3 || $count == $count_query) $html .=        '</div>
                                                            </div></div>';
                                if($count % 6 == 4 || $count % 6 == 5 || $count % 6 == 0) $html .='</div>';
                                if($count % 6 == 0 || $count == $count_query) $html .='</div>';
                            }
                        }
                        else{
                            if($count % 6 == 1){
                                $html .=    '<div class="row">';
                                $html .=        '<div class="col-md-4 col-sm-4 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-6">'.$small_item.'</div>';
                                if($count == $count_query) $html .= '<div></div></div>';
                            }
                            else{
                                if($count % 6 == 3) {
                                    $html .=    '<div class="col-md-8 col-sm-8 col-xs-12">'.$large_item.'</div>
                                                </div>';
                                }
                                else{
                                    if($count % 6 == 2) {
                                        $html .= '<div class="col-md-12 col-sm-12 col-xs-6">'.$small_item.'</div>';
                                        if($count == $count_query) $html .= '<div></div></div>';
                                        else $html .= '</div></div>';
                                    }
                                    else{
                                        if($count % 6 == 4) $html .= '<div class="row list9-3item">';
                                        $html .= '<div class="col-md-4 col-sm-4 col-xs-6">'.$small_item.'</div>';
                                        if($count % 6 == 0 || $count % 6 == 0) $html .= '</div>';
                                    }
                                }
                            }
                        }
                        $count++;
                        if($count % 6 == 0) $count_block++;
                    }
                }
                $html .=    '</div>';
                break;

            case 'home-8':
                if(empty($item) && empty($item_res)) $item_res = '0:2,480:3,767:4,800:3,980:4';
                if(empty($item)) $item = 4;
                if(empty($size)) $size = array(120,149);
                $html .=    '<div class="deal-slider8">';
                if(!empty($title)) $html .=    '<h2 class="title14 color">'.esc_html($title).'</h2>';
                $html .=        '<div class="wrap-item smart-slider" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        $html .=    '<div class="item-product8'.esc_attr($animation_class).'">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                        </div>
                                    </div>';
                    }
                }
                $html .=        '</div>
                            </div>';
                break;

            case 'tab-cat6':
            case 'product-box7':
                if(!empty($cats)){
                    if(empty($item) && empty($item_res)) $item_res = '0:1';
                    if(empty($item)) $item = 1;
                    if(empty($size)) $size = array(270,336);
                    $sizex = array(570,335);
                    $pre = rand(1,100);
                    $tabs = explode(",",$cats);
                    $tab_html = $tab_content = '';
                    foreach ($tabs as $key => $tab) {
                        $term = get_term_by( 'slug',$tab, 'product_cat' );
                        if(!empty($term) && is_object($term)){
                            if($key == 0) $active = 'active';
                            else $active = '';
                            $tab_html .=    '<li class="'.$active.'"><a href="#'.$pre.$term->slug.'" data-toggle="tab">'.$term->name.'</a></li>';
                            $tab_content .=    '<div class="tab-pane '.$active.'" id="'.$pre.$term->slug.'">
                                                    <div class="product-slider3">
                                                        <div class="wrap-item smart-slider arrow-style3" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                            unset($args['tax_query']);
                            $args['tax_query'][]=array(
                                'taxonomy'=>'product_cat',
                                'field'=>'slug',
                                'terms'=> $tab
                            );
                            $product_query = new WP_Query($args);
                            $count = 1;
                            $count_query = $product_query->post_count;
                            if($product_query->have_posts()) {
                                while($product_query->have_posts()) {
                                    $product_query->the_post();
                                    global $product;
                                    $large_item =   '<div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$sizex).'</a>
                                                                '.s7upf_product_link().'
                                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                '.s7upf_get_rating_html().'
                                                                '.s7upf_get_saleoff_html().'
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                '.s7upf_get_price_html().'
                                                            </div>
                                                        </div>
                                                    </div>';
                                    $small_item =   '<div class="col-md-3 col-sm-3 col-xs-6">
                                                        <div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                                '.s7upf_product_link().'
                                                                <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                '.s7upf_get_rating_html().'
                                                                '.s7upf_get_saleoff_html().'
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                '.s7upf_get_price_html().'
                                                            </div>
                                                        </div>
                                                    </div>';
                                    if($count % 6 == 1) $tab_content .= '<div class="item">';
                                    if($count % 3 == 1) $tab_content .= '<div class="row">';
                                    if($count % 3 == 2) $tab_content .= $large_item;
                                    else $tab_content .= $small_item;
                                    if($count_query == $count || $count % 3 == 0) $tab_content .=    '</div>';
                                    if($count % 6 == 0 || $count == $count_query) $tab_content .= '</div>';
                                    $count++;
                                }
                            }
                            $tab_content .=             '</div>
                                                    </div>
                                                </div>';
                        }
                    }
                    $html .=    '<div class="product-box6 '.esc_attr($style).'">';
                    if(!empty($title)) $html .=    '<div class="title-box1 text-center"><h2 class="title30"><span>'.esc_html($title).'</span></h2></div>';
                    $html .=        '<div class="title-box5">
                                        <ul class="list-none">
                                            '.$tab_html.'
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        '.$tab_content.'
                                    </div>
                                </div>';
                }
                break;

            default:
                if(empty($item) && empty($item_res)) $item_res = '0:1';
                if(empty($item)) $item = 1;
                if(empty($size)) $size = array(270,336);
                $sizex = array(570,335);
                $html .=    '<div class="product-box3">';
                if(!empty($title)) $html .=    '<div class="title-box1 text-center">
                                                    <h2 class="title30"><span>'.esc_html($title).'</span></h2>
                                                </div>';
                $html .=        '<div class="product-slider3">
                                    <div class="wrap-item smart-slider arrow-style3" data-item="'.esc_attr($item).'" data-speed="'.esc_attr($speed).'" data-itemres="'.esc_attr($item_res).'" data-prev="fa-arrow-circle-left" data-next="fa-arrow-circle-right" data-pagination="" data-navigation="true">';
                if($product_query->have_posts()) {
                    while($product_query->have_posts()) {
                        $product_query->the_post();
                        global $product;
                        $large_item =   '<div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                <div class="product-thumb">
                                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$sizex).'</a>
                                                    '.s7upf_product_link().'
                                                    <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                    '.s7upf_get_rating_html().'
                                                    '.s7upf_get_saleoff_html().'
                                                </div>
                                                <div class="product-info">
                                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                    '.s7upf_get_price_html().'
                                                </div>
                                            </div>
                                        </div>';
                        $small_item =   '<div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="item-product none-shadow'.esc_attr($animation_class).'">
                                                <div class="product-thumb">
                                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                    '.s7upf_product_link().'
                                                    <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                    '.s7upf_get_rating_html().'
                                                    '.s7upf_get_saleoff_html().'
                                                </div>
                                                <div class="product-info">
                                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                    '.s7upf_get_price_html().'
                                                </div>
                                            </div>
                                        </div>';
                        if($count % 6 == 1) $html .=    '<div class="item">';
                        if($count % 3 == 1) $html .=    '<div class="row">';
                        if($count % 6 == 1 || $count % 6 == 0) $html .= $large_item;
                        else $html .= $small_item;
                        if($count_query == $count || $count % 3 == 0) $html .=    '</div>';
                        if($count % 6 == 0 || $count_query == $count) $html .=    '</div>';
                        $count++;
                    }
                }
                $html .=            '</div>
                                </div>
                            </div>';
                break;
        }
        wp_reset_postdata();
        return $html;
    }
}

stp_reg_shortcode('sv_product_list','sv_vc_product_list');
add_action( 'vc_before_init_base','sv_add_list_product',10,100 );
if ( ! function_exists( 'sv_add_list_product' ) ) {
    function sv_add_list_product(){
        vc_map( array(
            "name"      => esc_html__("SV Product list", 'lucky'),
            "base"      => "sv_product_list",
            "icon"      => "icon-st",
            "category"  => '7Up-theme',
            "params"    => array(
                array(
                    'heading'     => esc_html__( 'Style', 'lucky' ),
                    'holder'      => 'div',
                    'type'        => 'dropdown',
                    'description' => esc_html__( 'Choose style to display.', 'lucky' ),
                    'param_name'  => 'style',
                    'value'       => array(                        
                        esc_html__('List home 3','lucky')     => 'home-3',
                        esc_html__('Tabs category 6','lucky')     => 'tab-cat6',
                        esc_html__('Tabs category 7','lucky')     => 'product-box7',
                        esc_html__('List home 8','lucky')     => 'home-8',
                        esc_html__('List home 9','lucky')     => 'home-9',
                        esc_html__('List home 10','lucky')     => 'home-10',
                        esc_html__('Tabs category 11','lucky')     => 'tab-home11',
                        esc_html__('List home 11','lucky')     => 'home-11',
                        esc_html__('Deal home 11','lucky')     => 'deal-home-11',
                        esc_html__('List side home 11','lucky')     => 'side-home-11',
                        esc_html__('Deal home 12','lucky')     => 'deal-home-12',
                        esc_html__('List home 12','lucky')     => 'list-home-12',
                        esc_html__('Tabs category 12','lucky')     => 'tab-home12',
                        esc_html__('List side home 12','lucky')     => 'side-home-12',
                        esc_html__('List home 14','lucky')     => 'list-home-14',
                        esc_html__('Tabs category 1','lucky')     => 'tab-home1',
                        )
                ),                
                array(
                    "type"          => "textfield",
                    "holder"        => "p",
                    "heading"       => esc_html__("Time Countdown",'lucky'),
                    'description'   => esc_html__( 'Entert time for countdown. Format is mm/dd/yyyy. Example: 12/15/2016', 'lucky' ),
                    "param_name"    => "time",
                    "dependency"    => array(
                        "element"       => "style",
                        "value"         => array("home-10",'deal-home-12'),
                        )
                ),
                array(
                    "type"          => "textfield",
                    "heading"       => esc_html__("View link",'lucky'),
                    "param_name"    => "view_link",
                    "dependency"    => array(
                        "element"       => "style",
                        "value"         => array('side-home-12'),
                        )
                ),
                array(
                    "type"          => "textfield",
                    "holder"        => "p",
                    "heading"       => esc_html__("Time Countdown",'lucky'),
                    'description'   => esc_html__( 'Enter time(hours:minutes) to countdown. Format is hh:mm. Example 18:30.', 'lucky' ),
                    "param_name"    => "time2",
                    "dependency"    => array(
                        "element"       => "style",
                        "value"         => array('deal-home-11'),
                        )
                ),
                array(
                    'type'        => 'textfield',
                    'holder'      => 'h3',
                    'heading'     => esc_html__( 'Title', 'lucky' ),
                    'param_name'  => 'title',
                ),
                array(
                    'heading'     => esc_html__( 'Number', 'lucky' ),
                    'type'        => 'textfield',
                    'description' => esc_html__( 'Enter number of product. Default is 8.', 'lucky' ),
                    'param_name'  => 'number',
                ),
                array(
                    'heading'     => esc_html__( 'Product Type', 'lucky' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'product_type',
                    'value' => array(
                        esc_html__('Default','lucky')            => '',
                        esc_html__('Trendding','lucky')          => 'trendding',
                        esc_html__('Featured Products','lucky')  => 'featured',
                        esc_html__('Best Sellers','lucky')       => 'bestsell',
                        esc_html__('On Sale','lucky')            => 'onsale',
                        esc_html__('Top rate','lucky')           => 'toprate',
                        esc_html__('Most view','lucky')          => 'mostview',
                    ),
                    'description' => esc_html__( 'Select Product View Type', 'lucky' ),
                ),
                array(
                    'holder'     => 'div',
                    'heading'     => esc_html__( 'Product Categories', 'lucky' ),
                    'type'        => 'checkbox',
                    'param_name'  => 'cats',
                    'value'       => s7upf_list_taxonomy('product_cat',false)
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Order By', 'lucky' ),
                    'value' => s7upf_get_order_list(),
                    'param_name' => 'orderby',
                    'description' => esc_html__( 'Select Orderby Type ', 'lucky' ),
                    'edit_field_class'=>'vc_col-sm-6 vc_column',
                ),
                array(
                    'heading'     => esc_html__( 'Order', 'lucky' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'order',
                    'value' => array(                   
                        esc_html__('Desc','lucky')  => 'DESC',
                        esc_html__('Asc','lucky')  => 'ASC',
                    ),
                    'description' => esc_html__( 'Select Order Type ', 'lucky' ),
                    'edit_field_class'=>'vc_col-sm-6 vc_column',
                ),
                array(
                    "type"          => "textfield",
                    "heading"       => esc_html__("Size Thumbnail",'lucky'),
                    "param_name"    => "size",
                    "group"         => esc_html__("Advanced",'lucky'),
                    'description' => esc_html__( 'Enter site thumbnail to crop. [width]x[height]. Example is 300x300', 'lucky' ),
                ),
                array(
                    "type"          => "textfield",
                    "heading"       => esc_html__("Item",'lucky'),
                    "param_name"    => "item",
                    "group"         => esc_html__("Advanced",'lucky'),
                ),
                array(
                    "type"          => "textfield",
                    "heading"       => esc_html__("Item Responsive",'lucky'),
                    "param_name"    => "item_res",
                    "group"         => esc_html__("Advanced",'lucky'),
                    'description' => esc_html__( 'Enter item for screen width(px) format is width:value and separate values by ",". Example is 0:2,600:3,1000:4. Default is auto.', 'lucky' ),
                ),
                array(
                    "type"          => "textfield",
                    "heading"       => esc_html__("Speed",'lucky'),
                    "param_name"    => "speed",
                    "group"         => esc_html__("Advanced",'lucky'),                    
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Animation', 'lucky' ),
                    'param_name' => 'animation',
                    'value' => s7upf_get_list_animation()
                ),
            )
        ));
    }
}
}