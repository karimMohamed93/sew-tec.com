<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_search_form'))
{
    function s7upf_vc_search_form($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'     => '',
            'placeholder'     => esc_html__("Find amazing products","lucky"),
            'live_search'       => 'on',
        ),$attr));
        ob_start();
        $search_val = get_search_query();
        if(empty($search_val)){
            $search_val = $placeholder;
        }
        ?>
        <div class="search-form <?php echo esc_attr($style)?> live-search-<?php echo esc_attr($live_search)?>">
            <form method="get" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                <input type="text" name="s" value="<?php echo esc_attr($search_val);?>" onfocus="if (this.value==this.defaultValue) this.value = ''" onblur="if (this.value=='') this.value = this.defaultValue">
                <input type="hidden" name="post_type" value="product" />
                <div class="submit-form">
                    <input value="" type="submit">
                </div>
                <div class="list-product-search">
                    <p class="text-center"><?php esc_html_e("Please enter key search to display results.","lucky")?></p>
                </div>
            </form>
        </div>
        <?php
        $html .=    ob_get_clean();
        return $html;
    }
}

stp_reg_shortcode('sv_search_form','s7upf_vc_search_form');

vc_map( array(
    "name"      => esc_html__("SV Search Form", 'lucky'),
    "base"      => "sv_search_form",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__("Default",'lucky')  => '',
            )
        ),        
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Live Search",'kuteshop'),
            "param_name" => "live_search",
            "value"     => array(
                esc_html__("On",'kuteshop')   => 'on',
                esc_html__("Off",'kuteshop')   => 'off',
                )
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => esc_html__("Place holder input",'kuteshop'),
            "param_name" => "placeholder",
        ),
    )
));
add_action( 'wp_ajax_live_search', 's7upf_live_search' );
add_action( 'wp_ajax_nopriv_live_search', 's7upf_live_search' );
if(!function_exists('s7upf_live_search')){
    function s7upf_live_search() {
        $key = $_POST['key'];
        $cat = $_POST['cat'];
        $post_type = $_POST['post_type'];
        $taxonomy = $_POST['taxonomy'];
        $trim_key = trim($key);
        $args = array(
            'post_type' => $post_type,
            's'         => $key,
            );
        if(!empty($cat)) {
            $args['tax_query'][]=array(
                'taxonomy'  =>  $taxonomy,
                'field'     =>  'slug',
                'terms'     =>  $cat
            );
        }
        $query = new WP_Query( $args );
        if( $query->have_posts() && !empty($key) && !empty($trim_key)){
            while ( $query->have_posts() ) : $query->the_post();
                global $product;
                echo    '<div class="item-search-pro">
                            <div class="search-ajax-thumb product-thumb">
                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">
                                    '.get_the_post_thumbnail(get_the_ID(),array(50,50)).'
                                </a>
                            </div>
                            <div class="search-ajax-title"><h3 class="title14"><a href="'.esc_url(get_the_permalink()).'">'.get_the_title().'</a></h3></div>
                            <div class="search-ajax-price">
                                '.s7upf_get_price_html().'
                            </div>
                        </div>';
            endwhile;
        }
        else{
            echo '<p class="text-center">'.esc_html__("No any results with this keyword.").'</p>';
        }
        wp_reset_postdata();
    }
}