<?php
/**
 * Created by Sublime Text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:20 AM
 */
if(class_exists("woocommerce")){
    if(function_exists('stp_reg_taxonomy')){
    	stp_reg_taxonomy(
    	    'product_brand',
    	    'product',
    	    array(
    	        'label' => esc_html__( 'Brands', 'lucky' ),
    	        'rewrite' => array( 'slug' => 'product_brand', 'lucky' ),
    	        'labels'	=> array(
    		        'all_items' => esc_html__( 'All Brands', 'lucky' ),
    		        'edit_item' => esc_html__( 'Edit Brand', 'lucky' ),
    		        'view_item' => esc_html__( 'View Brand', 'lucky' ),
    		        'update_item' => esc_html__( 'Update Brand', 'lucky' ),
    		        'add_new_item' => esc_html__( 'Add New Brand', 'lucky' ),
    		        'new_item_name' => esc_html__( 'New Brand Name', 'lucky' ),
    		       ),
    	        'hierarchical' => true,
    	        'query_var'  => true
    	    )
    	);
    }
	add_action( 'wp_ajax_add_to_cart', 's7upf_minicart_ajax' );
	add_action( 'wp_ajax_nopriv_add_to_cart', 's7upf_minicart_ajax' );
	if(!function_exists('s7upf_minicart_ajax')){
		function s7upf_minicart_ajax() {
			
			$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
			$quantity = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );
			$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

			if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) ) {
				do_action( 'woocommerce_ajax_added_to_cart', $product_id );
				WC_AJAX::get_refreshed_fragments();
			} else {
				$this->json_headers();

				// If there was an error adding to the cart, redirect to the product page to show any errors
				$data = array(
					'error' => true,
					'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
					);
				echo json_encode( $data );
			}
			die();
		}
	}
	/*********************************** END ADD TO CART AJAX ****************************************/

    /********************************** POPUP Wishlist ************************************/

    add_action( 'wp_ajax_custom_wishlist', 's7upf_custom_wishlist' );
    add_action( 'wp_ajax_nopriv_custom_wishlist', 's7upf_custom_wishlist' );
    if(!function_exists('s7upf_custom_wishlist')){
        function s7upf_custom_wishlist() {
            $product_id = $_POST['product_id'];
            $url = YITH_WCWL()->get_wishlist_url();
            echo    '<div class="wishlist-popup">
                        <span class="popup-icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
                        <p class="wishlist-alert">"'.get_the_title($product_id).'" '.esc_html__("was added to wishlist","lucky").'</p>
                        <div class="wishlist-button">
                            <a href="#">'.esc_html__("Close","lucky").' (<span class="wishlist-countdown">3</span>)</a>
                            <a href="'.esc_url($url).'">'.esc_html__("View page","lucky").'</a>
                        </div>
                    </div>';
        }
    }

    /********************************** Shop ajax ************************************/

    add_action( 'wp_ajax_load_shop', 's7upf_load_shop' );
    add_action( 'wp_ajax_nopriv_load_shop', 's7upf_load_shop' );
    if(!function_exists('s7upf_load_shop')){
        function s7upf_load_shop() {
            $data_filter = $_POST['filter_data'];
            // var_dump($data_filter);
            extract($data_filter);
            $args = array(
                'post_type'         => 'product',
                'posts_per_page'    => $number,
                'paged'             => $page,
            );
            $attr_taxquery = array();
            if(!empty($attributes)){                
                $attr_taxquery['relation'] = 'AND';
                $args['meta_query'][]  = array(
                    'key'           => '_visibility',
                    'value'         => array('catalog', 'visible'),
                    'compare'       => 'IN'
                );
                foreach($attributes as $attr => $term){
                    $attr_taxquery[] =  array(
                                            'taxonomy'      => 'pa_'.$attr,
                                            'terms'         => $term,
                                            'field'         => 'slug',
                                            'operator'      => 'IN'
                                        );
                }
            }
            if(!empty($cats)) {
                $attr_taxquery[]=array(
                    'taxonomy'=>'product_cat',
                    'field'=>'slug',
                    'terms'=> $cats
                );
            }
            if ( !empty($attr_taxquery)){                
                $args['tax_query'] = $attr_taxquery;
            }
            if( isset( $price['min']) && isset( $price['max']) ){
                $min = $price['min'];
                $max = $price['max'];
                if($max != $max_price || $min != $min_price) $args['post__in'] = s7upf_filter_price($min,$max);
            }
            switch ($orderby) {
                case 'price' :
                    $args['orderby']  = "meta_value_num ID";
                    $args['order']    = 'ASC';
                    $args['meta_key'] = '_price';
                break;

                case 'price-desc' :
                    $args['orderby']  = "meta_value_num ID";
                    $args['order']    = 'DESC';
                    $args['meta_key'] = '_price';
                break;

                case 'popularity' :
                    $args['meta_key'] = 'total_sales';
                    add_filter( 'posts_clauses', array( WC()->query, 'order_by_popularity_post_clauses' ) );
                break;

                case 'rating' :
                    $args['meta_key'] = '_wc_average_rating';
                    $args['orderby'] = 'meta_value_num';
                    $args['meta_query'] = WC()->query->get_meta_query();
                    $args['tax_query'][] = WC()->query->get_tax_query();
                break;

                case 'date':
                    $args['orderby'] = 'date';
                    break;
                
                default:
                    $args['orderby'] = 'menu_order';
                    break;
            }
            $grid_active = $list_active = '';
            if($type == 'grid') $grid_active = 'active'; 
            if($type == 'list') $list_active = 'active';
            $product_query = new WP_Query($args);
            $paged = ( isset($page) ) ? absint( $page ) : 1;
            ?>
            <div class="sort-pagi-bar top clearfix">
                <ul class="product-sort pull-left list-inline">
                    <li><a data-type="grid" href="<?php echo esc_url(s7upf_get_key_url('type','grid'))?>" class="load-shop-ajax grid <?php if($type == 'grid') echo 'active'?>"><?php esc_html_e("Grid","lucky")?></a></li>
                    <li><a data-type="list" href="<?php echo esc_url(s7upf_get_key_url('type','list'))?>" class="load-shop-ajax list <?php if($type == 'list') echo 'active'?>"><?php esc_html_e("List","lucky")?></a></li>
                </ul>
                <?php if($product_query->max_num_pages > 1){?>
                <div class="product-pagi-nav pull-right list-inline">
                    <?php
                        echo paginate_links( array(
                            'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
                            'format'       => '',
                            'add_args'     => '',
                            'current'      => max( 1, $paged ),
                            'total'        => $product_query->max_num_pages,
                            'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                            'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            'type'         => 'list',
                            'end_size'     => 2,
                            'mid_size'     => 1
                        ) );
                    ?>
                </div>
                <?php }?>
                <div class="product-filter pull-right">
                    <?php s7upf_catalog_ordering($product_query,$orderby)?>
                </div>
            </div>
            <div class="product-<?php echo esc_attr($type)?>">
                <ul class="list-product row list-unstyled" data-number="<?php echo esc_attr($number)?>" data-column="<?php echo esc_attr($column)?>" data-currency="<?php echo esc_attr(get_woocommerce_currency_symbol())?>">
            <?php
            $count_product = 1;
            if($product_query->have_posts()) {
                while($product_query->have_posts()) {
                    $product_query->the_post();
                    global $product;
                    ?>
                    <?php if($type == 'list'){
                        $list_text = get_the_excerpt();
                        ?>
                        <li class="col-md-12 col-sm-12 col-xs-12">  
                        <?php
                            echo '<div class="item-product clearfix">
                                        <div class="product-thumb">
                                            <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                            '.s7upf_product_link().'
                                            <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                            '.s7upf_get_rating_html().'
                                            <p class="desc">'.$list_text.'</p>
                                            <div class="wrap-cart-qty">
                                                '.s7upf_get_price_html().'
                                                <div class="info-extra product">
                                                    '.s7upf_wishlist_link().'
                                                    '.s7upf_compare_url().'
                                                </div>';
                                                woocommerce_template_single_add_to_cart();
                            echo            '</div>
                                        </div>
                                    </div>';
                        ?>
                    </li>
                    <?php }
                    else{
                        $col_option = $column;
                        switch ($col_option) {
                            case 1:
                                $size = 'full';
                                $col = 'col-md-12 col-sm-12';
                                break;

                            case 2:
                                $size = array(250*1.5,310*1.5);
                                $col = 'col-md-6 col-sm-6';
                                break;
                            
                            case 3:
                                $size = array(250*1.2,310*1.2);
                                $col = 'col-md-4 col-sm-6';
                                break;

                            case 6:
                                $col = 'col-md-2 col-sm-3';
                                $size = array(250,310);
                                break;

                            default:
                                $col = 'col-md-3 col-sm-4';
                                $size = array(250,310);
                                break;
                        }
                    ?>
                    <li class="<?php echo esc_attr($col)?> col-xs-6">
                        <?php
                            echo '<div class="item-product">
                                    <div class="product-thumb">
                                        <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                        '.s7upf_product_link().'
                                        <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                        '.s7upf_get_rating_html().'
                                        '.s7upf_get_saleoff_html().'
                                    </div>
                                    <div class="product-info">
                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                        '.s7upf_get_price_html().'
                                    </div>
                                </div>';
                        ?>
                    </li>
                    <?php 
                    $count_product++;
                    }
                }
            }
            ?>
                </ul>
            </div>
            <div class="sort-pagi-bar top clearfix">
                <ul class="product-sort pull-left list-inline">
                    <li><a data-type="grid" href="<?php echo esc_url(s7upf_get_key_url('type','grid'))?>" class="load-shop-ajax grid <?php if($type == 'grid') echo 'active'?>"><?php esc_html_e("Grid","lucky")?></a></li>
                    <li><a data-type="list" href="<?php echo esc_url(s7upf_get_key_url('type','list'))?>" class="load-shop-ajax list <?php if($type == 'list') echo 'active'?>"><?php esc_html_e("List","lucky")?></a></li>
                </ul>
                <?php if($product_query->max_num_pages > 1){?>
                <div class="product-pagi-nav pull-right list-inline">
                    <?php
                        echo paginate_links( array(
                            'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
                            'format'       => '',
                            'add_args'     => '',
                            'current'      => max( 1, $paged ),
                            'total'        => $product_query->max_num_pages,
                            'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                            'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            'type'         => 'list',
                            'end_size'     => 2,
                            'mid_size'     => 1
                        ) );
                    ?>
                </div>
                <?php }?>
                <div class="product-filter pull-right">
                    <?php s7upf_catalog_ordering($product_query,$orderby)?>
                </div>
            </div>
            <?php
            wp_reset_postdata();
        }
    }

	/********************************** REMOVE ITEM MINICART AJAX ************************************/

	add_action( 'wp_ajax_product_remove', 's7upf_product_remove' );
	add_action( 'wp_ajax_nopriv_product_remove', 's7upf_product_remove' );
	if(!function_exists('s7upf_product_remove')){
		function s7upf_product_remove() {
		    global $wpdb, $woocommerce;
		    $cart_item_key = $_POST['cart_item_key'];
		    if ( $woocommerce->cart->get_cart_item( $cart_item_key ) ) {
				$woocommerce->cart->remove_cart_item( $cart_item_key );
			}
		    exit();
		}
	}

	/********************************** HOOK ************************************/

	//remove woo breadcrumbs
    add_action( 'init','s7upf_remove_wc_breadcrumbs' );

    // Remove page title
    add_filter( 'woocommerce_show_page_title', 's7upf_remove_page_title');

	// remove action wrap main content
    remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
    remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

    // Custom wrap main content
    add_action('woocommerce_before_main_content', 's7upf_add_before_main_content', 10);
    add_action('woocommerce_after_main_content', 's7upf_add_after_main_content', 10);
    add_action('woocommerce_before_shop_loop', 's7upf_before_shop_loop', 10);
    add_action('woocommerce_after_shop_loop', 's7upf_after_shop_loop', 10);

    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
   	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
   	remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

   	add_filter( 'woocommerce_get_price_html', 's7upf_change_price_html', 100, 2 );

    if(!function_exists('s7upf_before_shop_loop')){
        function s7upf_before_shop_loop(){
            global $wp_query;
            $type = 'grid';
            if(isset($_GET['type'])){
                $type = $_GET['type'];
            }
            $column = s7upf_get_option('woo_shop_column',3);
            $number = s7upf_get_option('woo_shop_number',3);
            if(isset($_GET['column'])){
                    $column = $_GET['column'];
                }
            if(isset($_GET['number'])){
                $number = $_GET['number'];
            }
            ?>
            <div class="main-shop-load">
            <div class="sort-pagi-bar top clearfix">
                <ul class="product-sort pull-left list-inline">
                    <li><a data-type="grid" href="<?php echo esc_url(s7upf_get_key_url('type','grid'))?>" class="load-shop-ajax grid <?php if($type == 'grid') echo 'active'?>"><?php esc_html_e("Grid","lucky")?></a></li>
                    <li><a data-type="list" href="<?php echo esc_url(s7upf_get_key_url('type','list'))?>" class="load-shop-ajax list <?php if($type == 'list') echo 'active'?>"><?php esc_html_e("List","lucky")?></a></li>
                </ul>
                <div class="product-pagi-nav pull-right list-inline">
                    <?php
                        echo paginate_links( array(
                            'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
                            'format'       => '',
                            'add_args'     => '',
                            'current'      => max( 1, get_query_var( 'paged' ) ),
                            'total'        => $wp_query->max_num_pages,
                            'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                            'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            'type'         => 'list',
                            'end_size'     => 2,
                            'mid_size'     => 1
                        ) );
                    ?>
                </div>
                <div class="product-filter pull-right">
                    <?php woocommerce_catalog_ordering()?>
                </div>
            </div>
            <div class="product-<?php echo esc_attr($type)?>">
                <div class="list-product row list-unstyled" data-number="<?php echo esc_attr($number)?>" data-column="<?php echo esc_attr($column)?>" data-currency="<?php echo esc_attr(get_woocommerce_currency_symbol())?>">
            <?php
        }
    }

    if(!function_exists('s7upf_after_shop_loop')){
        function s7upf_after_shop_loop(){
            global $wp_query;
            $type = 'grid';
            if(isset($_GET['type'])){
                $type = $_GET['type'];
            }
            $column = s7upf_get_option('woo_shop_column',3);
            if(isset($_GET['column'])){
                    $column = $_GET['column'];
                }
            if(isset($_GET['number'])){
                $number = $_GET['number'];
            }
            ?>
                    </div>
                </div>
                <div class="sort-pagi-bar top clearfix">
                    <ul class="product-sort pull-left list-inline">
                        <li><a data-type="grid" href="<?php echo esc_url(s7upf_get_key_url('type','grid'))?>" class="load-shop-ajax grid <?php if($type == 'grid') echo 'active'?>"><?php esc_html_e("Grid","lucky")?></a></li>
                        <li><a data-type="list" href="<?php echo esc_url(s7upf_get_key_url('type','list'))?>" class="load-shop-ajax list <?php if($type == 'list') echo 'active'?>"><?php esc_html_e("List","lucky")?></a></li>
                    </ul>
                    <div class="product-pagi-nav pull-right list-inline">
                        <?php
                            echo paginate_links( array(
                                'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
                                'format'       => '',
                                'add_args'     => '',
                                'current'      => max( 1, get_query_var( 'paged' ) ),
                                'total'        => $wp_query->max_num_pages,
                                'prev_text'    => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                'next_text'    => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                'type'         => 'list',
                                'end_size'     => 2,
                                'mid_size'     => 1
                            ) );
                        ?>
                    </div>
                    <div class="product-filter pull-right">
                        <?php woocommerce_catalog_ordering()?>
                    </div>
                </div>
                </div>
            
            <?php
        }
    }

   	if(!function_exists('s7upf_change_price_html')){
    	function s7upf_change_price_html($price, $product){
    		$price = str_replace('&ndash;', '<span class="slipt">&ndash;</span>', $price);
    		$price = '<div class="product-price">'.$price.'</div>';
            $show_mode = s7upf_check_catelog_mode();
            $hide_price = s7upf_get_option('hide_price');
            if($show_mode == 'on' && $hide_price == 'on') $price = '';
    		return $price;
    	}
    }

    function s7upf_add_before_main_content() {
        $col_class = 'shop-width-'.s7upf_get_option('woo_shop_column',3);
        global $count_product;
        $count_product = 1;
        global $wp_query;
        $cats = '';
        if(isset($wp_query->query_vars['product_cat'])) $cats = $wp_query->query_vars['product_cat'];
        ?>
        <div id="main-content" class="shop-page <?php echo esc_attr($col_class);?>" data-cats="<?php echo esc_attr($cats);?>">
            <?php s7upf_header_image();?>
            <div class="container">
                <?php //s7upf_header_image();?>
            	<?php 
                $breadcrumb = s7upf_get_value_by_id('s7upf_show_breadrumb','on');
                if($breadcrumb == 'on'){
                	woocommerce_breadcrumb(array(
            			'delimiter'		=> '<i class="fa fa-caret-right" aria-hidden="true"></i>',
            			'wrap_before'	=> '<div class="bread-crumb radius">',
            			'wrap_after'	=> '</div>',
            		)); 
                }
        		?>
                <div class="row">
                	<?php s7upf_output_sidebar('left')?>
                	<div class="<?php echo esc_attr(s7upf_get_main_class()); ?>">
        <?php
    }

    function s7upf_add_after_main_content() {
        ?>
                	</div>
                	<?php s7upf_output_sidebar('right')?>
            	</div>
            </div>
        </div>
        <?php
    }

    function s7upf_remove_wc_breadcrumbs()
    {
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
    }

    function s7upf_remove_page_title() {
        return false;
    }
	/********************************* END REMOVE ITEM MINICART AJAX *********************************/

	/********************************** FANCYBOX POPUP CONTENT ************************************/

	add_action( 'wp_ajax_product_popup_content', 's7upf_product_popup_content' );
	add_action( 'wp_ajax_nopriv_product_popup_content', 's7upf_product_popup_content' );
	if(!function_exists('s7upf_product_popup_content')){
		function s7upf_product_popup_content() {
			$product_id = $_POST['product_id'];
			$query = new WP_Query( array(
				'post_type' => 'product',
				'post__in' => array($product_id)
				));
			if( $query->have_posts() ):
				echo '<div class="woocommerce single-product product-popup-content"><div class="product has-sidebar">';
				while ( $query->have_posts() ) : $query->the_post();	
					global $post,$product,$woocommerce;			
					s7upf_product_main_detai(true);
				endwhile;
				echo '</div></div>';
			endif;
			wp_reset_postdata();
		}
	}
	//Custom woo shop column
    add_filter( 'loop_shop_columns', 's7upf_woo_shop_columns', 1, 10 );
    function s7upf_woo_shop_columns( $number_columns ) {
        $col = s7upf_get_option('woo_shop_column',3);
        return $col;
    }
    add_filter( 'loop_shop_per_page', 's7upf_woo_shop_number', 20 );
    function s7upf_woo_shop_number( $number) {
        $col = s7upf_get_option('woo_shop_number',12);
        return $col;
    }
    // Image Header category Product
    add_action('product_cat_add_form_fields', 's7upf_product_cat_metabox_add', 10, 1);
    add_action('product_cat_edit_form_fields', 's7upf_product_cat_metabox_edit', 10, 1);    
    add_action('created_product_cat', 's7upf_product_save_category_metadata', 10, 1);    
    add_action('edited_product_cat', 's7upf_product_save_category_metadata', 10, 1);

    // Image Header category Post
    add_action('category_add_form_fields', 's7upf_product_cat_metabox_add', 10, 1);
    add_action('category_edit_form_fields', 's7upf_product_cat_metabox_edit', 10, 1);
    add_action('created_category', 's7upf_product_save_category_metadata', 10, 1);    
    add_action('edited_category', 's7upf_product_save_category_metadata', 10, 1);

    if(!function_exists('s7upf_product_cat_metabox_add')){ 
        function s7upf_product_cat_metabox_add($tag) { 
            ?>
            <div class="form-field">
                <label><?php esc_html_e('Category Header Image','lucky'); ?></label>
                <div class="wrap-metabox">
                    <div class="live-previews"></div>
                    <a class="button button-primary sv-button-remove"> <?php esc_html_e("Remove","lucky")?></a>
                    <a class="button button-primary sv-button-upload"><?php esc_html_e("Upload","lucky")?></a>
                    <input name="cat-header-image" type="hidden" class="sv-image-value" value=""></input>
                </div>
            </div>            
            <div class="form-field">
                <label><?php esc_html_e('Category Header Link','lucky'); ?></label>
                <input name="cat-header-link" type="text" value="" size="40">
            </div>
        <?php }
    }
    if(!function_exists('s7upf_product_cat_metabox_edit')){ 
        function s7upf_product_cat_metabox_edit($tag) { ?>
            <tr class="form-field">
                <th scope="row" valign="top">
                    <label><?php esc_html_e('Category Header Image','lucky'); ?></label>
                </th>
                <td>            
                    <div class="wrap-metabox">
                        <div class="live-previews">
                            <?php 
                                $image = get_term_meta($tag->term_id, 'cat-header-image', true);
                                echo '<img src="'.esc_url($image).'" />';
                            ?> 
                        </div>
                        <a class="button button-primary sv-button-remove"> <?php esc_html_e("Remove","lucky")?></a>
                        <a class="button button-primary sv-button-upload"><?php esc_html_e("Upload","lucky")?></a>
                        <input name="cat-header-image" type="hidden" class="sv-image-value" value=""></input>
                    </div>            
                </td>
            </tr>            
            <tr class="form-field">
                <th scope="row"><label><?php esc_html_e('Category Header Link','lucky'); ?></label></th>
                <td><input name="cat-header-link" type="text" value="<?php echo get_term_meta($tag->term_id, 'cat-header-link', true)?>" size="40">
            </tr>
        <?php }
    }
    if(!function_exists('s7upf_product_save_category_metadata')){ 
        function s7upf_product_save_category_metadata($term_id)
        {
            if (isset($_POST['cat-header-image'])) update_term_meta( $term_id, 'cat-header-image', $_POST['cat-header-image']);
            if (isset($_POST['cat-header-link'])) update_term_meta( $term_id, 'cat-header-link', $_POST['cat-header-link']);
        }
    }
    //end
    // Brand Product
    add_action('product_brand_add_form_fields', 's7upf_product_brand_metabox_add', 10, 1);
    add_action('product_brand_edit_form_fields', 's7upf_product_brand_metabox_edit', 10, 1);    
    add_action('created_product_brand', 's7upf_product_save_category_metadata', 10, 1);    
    add_action('edited_product_brand', 's7upf_product_save_category_metadata', 10, 1);

    if(!function_exists('s7upf_product_brand_metabox_add')){ 
        function s7upf_product_brand_metabox_add($tag) { 
            ?>
            <div class="form-field">
                <label><?php esc_html_e('Brand Image','lucky'); ?></label>
                <div class="wrap-metabox">
                    <div class="live-previews"></div>
                    <a class="button button-primary sv-button-remove"> <?php esc_html_e("Remove","lucky")?></a>
                    <a class="button button-primary sv-button-upload"><?php esc_html_e("Upload","lucky")?></a>
                    <input name="brand-image" type="hidden" class="sv-image-value" value=""></input>
                </div>
            </div>
        <?php }
    }
    if(!function_exists('s7upf_product_brand_metabox_edit')){ 
        function s7upf_product_brand_metabox_edit($tag) { ?>
            <tr class="form-field">
                <th scope="row" valign="top">
                    <label><?php esc_html_e('Brand Image','lucky'); ?></label>
                </th>
                <td>            
                    <div class="wrap-metabox">
                        <div class="live-previews">
                            <?php 
                                $image = get_term_meta($tag->term_id, 'cat-header-image', true);
                                echo '<img src="'.esc_url($image).'" />';
                            ?> 
                        </div>
                        <a class="button button-primary sv-button-remove"> <?php esc_html_e("Remove","lucky")?></a>
                        <a class="button button-primary sv-button-upload"><?php esc_html_e("Upload","lucky")?></a>
                        <input name="brand-image" type="hidden" class="sv-image-value" value=""></input>
                    </div>            
                </td>
            </tr>
        <?php }
    }
    if(!function_exists('s7upf_product_save_category_metadata')){ 
        function s7upf_product_save_category_metadata($term_id)
        {
            if (isset($_POST['brand-image'])) update_term_meta( $term_id, 'brand-image', $_POST['brand-image']);
        }
    }
    //end
    // Gitem filter
    add_filter('vc_gitem_add_link_param', 's7upf_add_grid_link');
    if(!function_exists('s7upf_add_grid_link')){
        function s7upf_add_grid_link($data){
            $data['value'][esc_html__( 'Wishlist link', 'lucky' )] = 'wishlist';
            $data['value'][esc_html__( 'Compare link', 'lucky' )] = 'compare';
            $data['value'][esc_html__( 'Quick view link', 'lucky' )] = 'quickview';
            return $data;
        }
    }
    add_filter( 'vc_gitem_post_data_get_link_link', 's7upf_gitem_post_data_get_link_link',10,2 );
    if(!function_exists('s7upf_gitem_post_data_get_link_link')){
        function s7upf_gitem_post_data_get_link_link($link, $atts, $css_class){
            if ( isset( $atts['link'] )){
                switch ($atts['link']) {
                    case 'compare':
                        $link = 'a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( array('action' => 'yith-woocompare-add-product','id' => '' )))).'={{ woocommerce_product:id }}" class="product-compare compare compare-link vc_gitem-link vc_icon_element-link" data-product_id="{{ woocommerce_product:id }}"></a';
                        break;
                    
                    case 'wishlist':
                        if(class_exists('YITH_WCWL_Init')) $link = 'a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'add_to_wishlist', '' ))).'={{ woocommerce_product:id }}" class="add_to_wishlist wishlist-link vc_gitem-link vc_icon_element-link" data-product-id="{{ woocommerce_product:id }}"></a';
                        break;

                    case 'quickview':
                        $link = 'a data-product-id="{{ woocommerce_product:id }}" href="{{ woocommerce_product_link }}" class="product-quick-view quickview-link vc_gitem-link vc_icon_element-link"></a';
                        break;

                    default:
                        
                        break;
                }
            }
            return $link;
        }
    }
    add_filter( 'vc_grid_item_shortcodes', 's7upf_grid_item_shortcodes',10,2 );
    if(!function_exists('s7upf_grid_item_shortcodes')){
        function s7upf_grid_item_shortcodes($shortcodes){
            $custom_shortcode = array(
                '7upf_rating' => array(
                    'name' => esc_html__( 'WooCommerce hover', 'lucky' ),
                    'base' => '7upf_hover',
                    'icon' => 'icon-wpb-woocommerce',
                    'category' => esc_html__( 'Content', 'lucky' ),
                    'description' => esc_html__( 'Woocommerce Rating', 'lucky' ),
                    'php_class_name' => 'Vc_Gitem_Woocommerce_Shortcode',
                    'params' => array(
                        array(
                            "type" => "dropdown",
                            "heading" => esc_html__("Style",'lucky'),
                            "param_name" => "style",
                            "value"     => array(
                                esc_html__("Default",'lucky')   => '',
                                )
                        ),
                    ),
                    'post_type' => Vc_Grid_Item_Editor::postType(),
                )
            );
            $shortcodes += $custom_shortcode;
            return $shortcodes;
        }
    }
    if(!function_exists('s7upf_vc_hover'))
    {
        function s7upf_vc_hover($attr)
        {
            $html = '';        
            extract(shortcode_atts(array(
                'style'      => '',
            ),$attr));
            $html .=    '<div class="product-extra-overlay"></div>';
            return $html;
        }
    }

    if(function_exists('stp_reg_shortcode')) stp_reg_shortcode('7upf_hover','s7upf_vc_hover');

    add_filter( 'vc_gitem_template_attribute_woocommerce_product', 's7upf_gitem_template_attribute_woocommerce_product', 100, 99 );
    function s7upf_gitem_template_attribute_woocommerce_product($value, $data){
        extract( array_merge( array(
            'post' => null,
            'data' => ''
        ), $data ) );
        $atts = array();
        parse_str( $data, $atts );
        $html = '';
        switch ($data) {
            case 'ratting_html':
                $id = $post->ID;
                $product = wc_get_product( $id );
                $star = $product->get_average_rating();
                $review_count = $product->get_review_count();
                $width = $star / 5 * 100;
                $html .=    '<div class="product-rate">
                                <div class="product-rating" style="width:'.$width.'%;"></div>';
                $html .=    '</div>';
                return $html;
                break;

            case 'ratting_hover':
                $id = $post->ID;
                $product = wc_get_product( $id );
                $star = $product->get_average_rating();
                $review_count = $product->get_review_count();
                $width = $star / 5 * 100;
                $html .=    '<div class="product-rate gitem-hover">
                                <div class="product-rating" style="width:'.$width.'%;"></div>';
                $html .=    '</div>';
                return $html;
                break;

            case 'quick_view_hover':
                $id = $post->ID;
                $html = '<a data-product-id="'.esc_attr($id).'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link quickview-gitem-hover"><i class="fa fa-search" aria-hidden="true"></i></a>';
                return $html;
                break;

            case 'meta_links_html':
                $id = $post->ID;
                $product = wc_get_product( $id );
                $html .=    '<div class="product-extra-link product-extra-link-hover hidden-text product">
                                <a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( array('action' => 'yith-woocompare-add-product','id' => $product->get_id() )))).'" class="product-compare compare compare-link vc_gitem-link vc_icon_element-link" data-product_id="'.esc_attr($product->get_id()).'"><i class="fa fa-file-o" aria-hidden="true"></i><span>'.esc_html__("Compare","lucky").'</span></a>
                                '.s7upf_addtocart_link($product).'
                                '.s7upf_wishlist_link($product->get_id()).'
                            </div>';
                return $html;
                break;

            case 'hover-overlay':
                return '<div class="product-extra-overlay"></div>';
                break;

            case 's7upf_price_html':
                $id = $post->ID;
                $product = wc_get_product( $id );
                return $product->get_price_html();
                break;

            case 'product_html':
                $id = $post->ID;
                $product = wc_get_product( $id );
                $star = $product->get_average_rating();
                $review_count = $product->get_review_count();
                $width = $star / 5 * 100;
                $from = $product->get_regular_price();
                $to = $product->get_price();
                $date_pro = strtotime($post->post_date);
                $date_now = strtotime('now');
                $set_timer = s7upf_get_option( 'sv_set_time_woo', 30);
                $uppsell = ($date_now - $date_pro - $set_timer*24*60*60);
                $percent = $meta_html = '';
                if($from != $to && $from > 0){
                    $percent = round(($from-$to)/$from*100);            
                    $meta_html .= '<span class="product-label sale-label">-'.$percent.'%</span>';
                }
                if($uppsell < 0) $meta_html .=  '<span class="product-label new-label">'.esc_html__("new","lucky").'</span>';
                $html .=    '<div class="item-product item-gitem">
                                <div class="product-thumb">
                                    <a href="'.esc_url(get_the_permalink($id)).'" class="product-thumb-link">'.get_the_post_thumbnail($id,array(300,300)).'</a>
                                    <div class="product-extra-link hidden-text product">
                                        <a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( array('action' => 'yith-woocompare-add-product','id' => $product->get_id() )))).'" class="product-compare compare compare-link vc_gitem-link vc_icon_element-link" data-product_id="'.esc_attr($product->get_id()).'"><i class="fa fa-file-o" aria-hidden="true"></i><span>'.esc_html__("Compare","lucky").'</span></a>
                                        '.s7upf_addtocart_link($product).'
                                        '.s7upf_wishlist_link($product->get_id()).'
                                    </div>
                                    <a data-product-id="'.esc_attr($id).'" href="'.esc_url(get_the_permalink($id)).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <div class="product-rate">
                                        <div class="product-rating" style="width:'.$width.'%;"></div>
                                    </div>
                                    '.$meta_html.'
                                </div>
                            </div>';
                return $html;
                break;

            case 'product_html2':
                $id = $post->ID;
                $product = wc_get_product( $id );
                $star = $product->get_average_rating();
                $review_count = $product->get_review_count();
                $width = $star / 5 * 100;
                $html .=    '<div class="item-product-masonry item-gitem">
                                <div class="product-thumb">
                                    <a href="'.esc_url(get_the_permalink($id)).'" class="product-thumb-link">'.get_the_post_thumbnail($id,array(300,300)).'</a>
                                    <div class="product-info">
                                        <div class="product-extra-link hidden-text product">
                                            <a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( array('action' => 'yith-woocompare-add-product','id' => $product->get_id() )))).'" class="product-compare compare compare-link vc_gitem-link vc_icon_element-link" data-product_id="'.esc_attr($product->get_id()).'"><i class="fa fa-file-o" aria-hidden="true"></i><span>'.esc_html__("Compare","lucky").'</span></a>
                                            '.s7upf_addtocart_link($product).'
                                            '.s7upf_wishlist_link($product->get_id()).'
                                        </div>
                                        <div class="product-rate">
                                            <div class="product-rating" style="width:'.$width.'%;"></div>
                                        </div>
                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink($id)).'" title="'.esc_attr(get_the_title($id)).'">'.get_the_title($id).'</a></h3>
                                        '.$product->get_price_html().'
                                    </div>
                                </div>
                            </div>';
                return $html;
                break;

            case 'product_html3':
                $id = $post->ID;
                $product = wc_get_product( $id );
                $star = $product->get_average_rating();
                $review_count = $product->get_review_count();
                $width = $star / 5 * 100;
                $html .=    '<div class="item-product8 item-gitem">
                                <div class="product-thumb">
                                    <a href="'.esc_url(get_the_permalink($id)).'" class="product-thumb-link">'.get_the_post_thumbnail($id,array(300,300)).'</a>
                                    <a data-product-id="'.esc_attr($id).'" href="'.esc_url(get_the_permalink($id)).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div>
                            </div>';
                return $html;
                break;
            
            default:
                return $value;
                break;
        }   
    }
    // Catalog mode
    add_filter( 's7upf_tempalte_mini_cart', 's7upf_tempalte_mini_cart', 100, 2 );
    if(!function_exists('s7upf_tempalte_mini_cart')){
        function s7upf_tempalte_mini_cart($html){
            $show_mode = s7upf_check_catelog_mode();
            $hide_minicart = s7upf_get_option('hide_minicart');
            if($show_mode == 'on' && $hide_minicart == 'on') $html = '';
            return $html;
        }
    }
    add_filter( 'woocommerce_loop_add_to_cart_link', 's7upf_custom_add_to_cart_link' );
    if(!function_exists('s7upf_custom_add_to_cart_link')){
        function s7upf_custom_add_to_cart_link($content){
            $show_mode = s7upf_check_catelog_mode();
            if($show_mode == 'on') $content = '';
            return $content;
        }
    }
    add_action( 's7upf_template_single_add_to_cart', 'woocommerce_template_single_add_to_cart', 30 );
    add_action( 's7upf_template_single_add_to_cart', 's7upf_filter_single_add_to_cart', 20 );
    // Catalog mode function
    if(!function_exists('s7upf_check_catelog_mode')){
        function s7upf_check_catelog_mode(){
            $catelog_mode = s7upf_get_option('woo_catelog');
            $hide_other_page = s7upf_get_option('hide_other_page');
            $hide_detail = s7upf_get_option('hide_detail');
            $hide_admin = s7upf_get_option('hide_admin');
            $hide_shop = s7upf_get_option('hide_shop');
            $hide_price = s7upf_get_option('hide_price');
            $show_mode = 'off';
            if($catelog_mode == 'on'){
                if($hide_other_page == 'on' && !is_super_admin() && !is_shop() && !is_single()) $show_mode = 'on';
                if($hide_other_page == 'on' && $hide_admin == 'on' && is_super_admin() && !is_shop() && !is_single() ) $show_mode = 'on';
                if(is_shop()) {
                    if($hide_shop == 'on' && !is_super_admin()) $show_mode = 'on';
                    if($hide_shop == 'on' && $hide_admin == 'on' && is_super_admin()) $show_mode = 'on';
                }
                if(is_single()) {
                    if($hide_detail == 'on' && !is_super_admin()) $show_mode = 'on';
                    if($hide_detail == 'on' && $hide_admin == 'on' && is_super_admin()) $show_mode = 'on';
                }
            }
            return $show_mode;
        }
    }
    if(!function_exists('s7upf_filter_single_add_to_cart')){
        function s7upf_filter_single_add_to_cart(){
            $show_mode = s7upf_check_catelog_mode();
            if($show_mode == 'on'){
                S7upf_Assets::add_css('.product-available,.product-code{display:none;}');
                remove_action( 's7upf_template_single_add_to_cart', 'woocommerce_template_single_add_to_cart', 30);
            }
        }
    }
}