(function($){
    "use strict"; // Start of use strict  

    /************** FUNCTION ****************/ 
    function set_pos_megamenu(){
        if($('.main-nav:not(.main-nav8)').length > 0 && !$('.main-nav').parents('.header-nav6').length > 0){
        	$('li.has-mega-menu > .sub-menu').attr('style','');
            if($('.rtl-enable').length > 0){
                var menu_pos = $(window).width() - ($('.header .container').offset().left + $('.header .container').outerWidth());
                $('li.has-mega-menu').each(function(){
                    var sub_pos = $(window).width() - ($(this).offset().left + $(this).outerWidth()+165);
                    console.log($(this).offset().left);
                    $(this).find('> .sub-menu').css('right',menu_pos-sub_pos);
                })
            }
            else{
                var menu_pos = $('.header .container').offset().left+15;
                $('li.has-mega-menu').each(function(){
                    var sub_pos = $(this).offset().left;
                    $(this).find('> .sub-menu').css('left',menu_pos-sub_pos);
                })
            }
        }
    }
    function fix_compare_link(){
    	$('body .compare-link.vc_gitem-link').each(function(){
    		$(this).parent().addClass('product');
    	})
    }
    // Menu fixed
    function fixed_header(){
        var menu_element;
        menu_element = $('.main-nav:not(.menu-fixed-content)').closest('.vc_row');
        if($('.menu-fixed-enable').length > 0 && $(window).width()>1024){           
            var menu_class = $('.main-nav').attr('class');
            var header_height = $("#header").height()+100;
            var ht = header_height + 150;
            var st = $(window).scrollTop();

            if(!menu_element.hasClass('header-fixed') && menu_element.attr('data-vc-full-width') == 'true') menu_element.addClass('header-fixed');
            if(st>header_height){               
                if(menu_element.attr('data-vc-full-width') == 'true'){
                    if(st > ht) menu_element.addClass('active');
                    else menu_element.removeClass('active');
                    menu_element.addClass('fixed-header');
                }
                else{
                    if(st > ht) menu_element.parent().parent().addClass('active');
                    else menu_element.parent().parent().removeClass('active');
                    if(!menu_element.parent().parent().hasClass('fixed-header')){
                        menu_element.wrap( "<div class='menu-fixed-content fixed-header "+menu_class+"'><div class='container'></div></div>" );
                    }
                }
            }else{
                menu_element.removeClass('active');
                if(menu_element.attr('data-vc-full-width') == 'true') menu_element.removeClass('fixed-header');
                else{
                    if(menu_element.parent().parent().hasClass('fixed-header')){
                        menu_element.unwrap();
                        menu_element.unwrap();
                    }
                }
            }
        }
        else{
            menu_element.removeClass('active');
            if(menu_element.attr('data-vc-full-width') == 'true') menu_element.removeClass('fixed-header');
            else{
                if(menu_element.parent().parent().hasClass('fixed-header')){
                    menu_element.unwrap();
                    menu_element.unwrap();
                }
            }
        }
    }
    //Menu Responsive
	function rep_menu(){
		$('.toggle-mobile-menu').on('click',function(event){
			event.preventDefault();
			$(this).parents('.main-nav').toggleClass('active');
		});
		$('.main-nav li.menu-item-has-children>a').on('click',function(event){
			if($(window).width()<768){
				event.preventDefault();
				$(this).next().stop(true,false).slideToggle();
			}else{
				// return false;
			}
		});
	}
	//Offset Menu
	function offset_menu(){
		if($(window).width()>767){
			$('.main-nav .sub-menu').each(function(){
				var wdm = $(window).width();
				var wde = $(this).width();
				var offset = $(this).offset().left;
				var tw = offset+wde;
				if(tw>wdm){
					$(this).addClass('offset-right');
				}
			});
		}else{
			return false;
		}
	}

    function slick_animated(seff){
    	var wrap = $(seff.currentTarget);
    	var item = wrap.find('.item-slider').find('.banner-info');
    	var item_active = wrap.find('.slick-active').find('.banner-info');
    	item.each(function(){    		
    		$(this).removeClass($(this).attr('data-anim-type'));
    	})
    	item_active.addClass(item_active.attr('data-anim-type'));
	}
	function slick_control(){
		$('.slick-slider').each(function(){
			$(this).find('.slick-prev-img').html($('.slick-active').prev().find('.banner-thumb a').html());
			$(this).find('.slick-next-img').html($('.slick-active').next().find('.banner-thumb a').html());
		});
	}

    function background(){
		$('.bg-slider .item-slider').each(function(){
			var src=$(this).find('.banner-thumb a img').attr('src');
			$(this).css('background-image','url("'+src+'")');
		});	
	}
    function menu_responsive(){
    	//Menu Responsive
		$('body').on('click',function(event){
			if($(window).width()<768){
				$('.main-nav>ul').removeClass('active');
			}
		});
		$('.toggle-mobile-menu').on('click',function(event){
			if($(window).width()<768){
				event.preventDefault();
				event.stopPropagation();
				$('.main-nav>ul').toggleClass('active');
			}
		});
		$('.main-nav li.menu-item-has-children>a').on('click',function(event){
			if($(window).width()<768 && !$(this).parent().hasClass('has-mega-menu')){
				event.preventDefault();
				event.stopPropagation();
				$(this).next().slideToggle('slow');
			}
		});

		$('.menu-hover-link').on('click',function(event){
			event.preventDefault();
			$('.content-menu-hover').toggleClass('active');
		});
    }
    
    function fix_variable_product(){
    	//Fix product variable thumb
    	$('input[name="variation_id"]').on('change',function(){
        	var id = $(this).val();
            var data = $('.variations_form').attr('data-product_variations');
            var curent_data = {};
            data = $.parseJSON(data);
            if(id){
            	for (var i = data.length - 1; i >= 0; i--) {
					if(data[i].variation_id == id) curent_data = data[i];
					if(data[i].is_in_stock) $('.product-available .avail-instock').html($('.product-available').attr('data-instock')).css("color","#72b226");
        			else $('.product-available .avail-instock').html($('.product-available').attr('data-outstock')).css("color","#ff0000");
        			$('.product-code span').html(data[i].sku);
				};
				if('image_id' in curent_data){
					$('.product-gallery #bx-pager').find('a[data-image_id="'+curent_data.image_id+'"]').trigger( 'click' );
                }
            }          
        })
        // variable product
        if($('.wrap-attr-product1.special').length > 0){
            $('.attr-filter ul li a').live('click',function(event){
                event.preventDefault();
                $(this).parents('ul').find('li').removeClass('active');
                $(this).parent().addClass('active');
                var attribute = $(this).parent().attr('data-attribute');
                var id = $(this).parents('ul').attr('data-attribute-id');
                $('#'+id).val(attribute);
                $('#'+id).trigger( 'change' );
                $('#'+id).trigger( 'focusin' );
                return false;
            })
            $('.attr-hover-box').hover(function(){
                var seff = $(this);
                var old_html = $(this).find('ul').html();
                var current_val = $(this).find('ul li.active').attr('data-attribute');
                $(this).next().find('select').trigger( 'focusin' );
                var content = '';
                $(this).next().find('select').find('option').each(function(){
                    var val = $(this).attr('value');
                    var title = $(this).html();
                    var el_class = '';
                    if(current_val == val) el_class = ' class="active"';
                    if(val != ''){
                        content += '<li'+el_class+' data-attribute="'+val+'"><a href="#" class="bgcolor-'+val+'"><span></span>'+title+'</a></li>';
                    }
                })
                // console.log(content);
                if(old_html != content) $(this).find('ul').html(content);
            })
            $('body .reset_variations').live('click',function(){
                $('.attr-hover-box').each(function(){
                    var seff = $(this);
                    var old_html = $(this).find('ul').html();
                    var current_val = $(this).find('ul li.active').attr('data-attribute');
                    $(this).next().find('select').trigger( 'focusin' );
                    var content = '';
                    $(this).next().find('select').find('option').each(function(){
                        var val = $(this).attr('value');
                        var title = $(this).html();
                        var el_class = '';
                        if(current_val == val) el_class = ' class="active"';
                        if(val != ''){
	                        content += '<li'+el_class+' data-attribute="'+val+'"><a href="#" class="bgcolor-'+val+'"><span></span>'+title+'</a></li>';
	                    }
                    })
                    if(old_html != content) $(this).find('ul').html(content);
                    $(this).find('ul li').removeClass('active');
                })
            })
        }
        //end
    }
    
    function afterAction(){
		this.$elem.find('.owl-item').removeClass('active');
		this.$elem.find('.owl-item').eq(this.owl.currentItem).addClass('active');
		this.$elem.find('.owl-item').each(function(){
			// $(this).find('.wow').removeClass('animated');
			var check = $(this).hasClass('active');
			if(check==true){
				$(this).find('.animated').each(function(){
					var anime = $(this).attr('data-anim-type');
					$(this).addClass(anime);
				});
			}else{
				$(this).find('.animated').each(function(){
					var anime = $(this).attr('data-anim-type');
					$(this).removeClass(anime);
				});
			}
		})
	}
    function s7upf_qty_click(){
    	//QUANTITY CLICK
		$(".quantity").find(".qty-up").on("click",function(){
            var min = $(this).prev().attr("data-min");
            var max = $(this).prev().attr("data-max");
            var step = $(this).prev().attr("data-step");
            if(step === undefined) step = 1;
            if(max !==undefined && Number($(this).prev().val())< Number(max) || max === undefined){ 
                if(step!='') $(this).prev().val(Number($(this).prev().val())+Number(step));
            }            
            $( 'div.woocommerce > form input[name="update_cart"]' ).prop( 'disabled', false );
            return false;
        })
        $(".quantity").find(".qty-down").on("click",function(){
            var min = $(this).next().attr("data-min");
            var max = $(this).next().attr("data-max");
            var step = $(this).next().attr("data-step");
            if(step === undefined) step = 1;
            if(Number($(this).next().val()) > 1){
	            if(min !==undefined && $(this).next().val()>min || min === undefined){
	                if(step!='') $(this).next().val(Number($(this).next().val())-Number(step));
	            }
	        }
            $( 'div.woocommerce > form input[name="update_cart"]' ).prop( 'disabled', false );
	        return false;
        })
        $("input.qty-val").on("keyup change",function(){
        	var max = $(this).attr('data-max');
        	if( Number($(this).val()) > Number(max) ) $(this).val(max);
            $( 'div.woocommerce > form input[name="update_cart"]' ).prop( 'disabled', false );
        })
		//END
    }
    
    function s7upf_owl_slider(){
    	//Carousel Slider
		if($('.sv-slider').length>0){
			$('.sv-slider').each(function(){
				var seff = $(this);
				var item = seff.attr('data-item');
				var speed = seff.attr('data-speed');
				var itemres = seff.attr('data-itemres');
				var animation = seff.attr('data-animation');
				var nav = seff.attr('data-nav');
				var text_prev = seff.attr('data-prev');
				var text_next = seff.attr('data-next');
				var pagination = false, navigation= true, singleItem = false;
				var autoplay;
				if(speed != '') autoplay = speed;
				else autoplay = false;
				// Navigation
				if(nav == 'nav-hidden'){
					pagination = false;
					navigation= false;
				}
				if(nav == 'banner-slider3' || nav == 'about-testimo-slider'){
					pagination = true;
					navigation= false;
				}
				if(nav == 'banner-slider2 banner-slider11'){
					pagination = true;
					navigation= true;
				}
				if(animation != ''){
					singleItem = true;
					item = '1';
				}
				var prev_text = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
				var next_text = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
				if(nav == 'nav-text-data'){
					var prev_text = text_prev;
					var next_text = text_next;
				}
				if(nav == 'banner-slider2' || nav == 'banner-slider13' || nav == 'about-service-slider' || nav == 'banner-slider9 long-arrow' || nav == 'banner-slider10' || nav == 'hotcat-slider14 arrow-style14' || nav == 'banner-slider2 banner-slider11'){
					prev_text = '<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>';
					next_text = '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>';
				}
				if(itemres == '' || itemres === undefined){
					if(item == '1') itemres = '0:1,480:1,768:1,1200:1';
					if(item == '2') itemres = '0:1,480:1,768:2,1200:2';
					if(item == '3') itemres = '0:1,480:2,768:2,1200:3';
					if(item == '4') itemres = '0:1,480:2,768:2,1200:4';
					if(item >= '5') itemres = '0:1,480:2,568:3,1024:5,1200:'+item;
				}				
				itemres = itemres.split(',');
				var i;
				for (i = 0; i < itemres.length; i++) { 
				    itemres[i] = itemres[i].split(':');
				}
				seff.owlCarousel({
					items: item,
					itemsCustom: itemres,
					autoPlay:autoplay,
					pagination: pagination,
					navigation: navigation,
					navigationText:[prev_text,next_text],
					singleItem : singleItem,
					beforeInit:background,
					// addClassActive : true,
					afterAction: afterAction,
					transitionStyle : animation
				});
			});			
		}
    }

    function s7upf_all_slider(){
    	//Carousel Slider
		if($('.smart-slider').length>0){
			$('.smart-slider').each(function(){
				var seff = $(this);
				var item = seff.attr('data-item');
				var speed = seff.attr('data-speed');
				var itemres = seff.attr('data-itemres');
				var text_prev = seff.attr('data-prev');
				var text_next = seff.attr('data-next');
				var pagination = seff.attr('data-pagination');
				var navigation = seff.attr('data-navigation');
				var paginumber = seff.attr('data-paginumber');
				var autoplay;
				if(speed === undefined) speed = '';
				if(speed != '') autoplay = speed;
				else autoplay = false;
				if(item == '' || item === undefined) item = 1;
				if(itemres === undefined) itemres = '';
				if(text_prev == 'false') text_prev = '';
				else{
					if(text_prev == '' || text_prev === undefined) text_prev = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
					else text_prev = '<i class="fa '+text_prev+'" aria-hidden="true"></i>';
				}
				if(text_next == 'false') text_next = '';
				else{
					if(text_next == '' || text_next === undefined) text_next = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
					else text_next = '<i class="fa '+text_next+' aria-hidden="true"></i>';
				}
				if(pagination == 'true') pagination = true;
				else pagination = false;
				if(navigation == 'true') navigation = true;
				else navigation = false;
				if(paginumber == 'true') paginumber = true;
				else paginumber = false;
				// Item responsive
				if(itemres == '' || itemres === undefined){
					if(item == '1') itemres = '0:1,480:1,768:1,1024:1';
					if(item == '2') itemres = '0:1,480:1,768:2,1024:2';
					if(item == '3') itemres = '0:1,480:2,768:2,1024:3';
					if(item == '4') itemres = '0:1,480:2,768:2,1024:4';
					if(item >= '5') itemres = '0:1,480:2,568:3,1024:'+item;
				}				
				itemres = itemres.split(',');
				var i;
				for (i = 0; i < itemres.length; i++) { 
				    itemres[i] = itemres[i].split(':');
				}
				seff.owlCarousel({
					items: item,
					itemsCustom: itemres,
					autoPlay:autoplay,
					pagination: pagination,
					navigation: navigation,
					navigationText:[text_prev,text_next],
					paginationNumbers:paginumber,
					// addClassActive : true,
					// afterAction: afterAction,
				});
			});			
		}
    }

    /************ END FUNCTION **************/  
	$(document).ready(function(){
		$('.product-gallery .bxslider img').on('hover',function(){
			$.removeData($('.product-gallery .bxslider img'), 'elevateZoom');//remove zoom instance from image
			$('.zoomContainer').remove();
			$(this).elevateZoom({
				zoomType: "inner",
				cursor: "crosshair",
				zoomWindowFadeIn: 500,
				zoomWindowFadeOut: 750
			});
		})		
		// menu_responsive();
		//Menu Responsive 
		rep_menu();
		//Offset Menu
		offset_menu();
		s7upf_qty_click();
		fix_variable_product();
		$('.post-thumb-zoom').fancybox();
		//Fix mailchimp
        $('.sv-mailchimp-form').each(function(){
            var placeholder = $(this).attr('data-placeholder');
            var submit = $(this).attr('data-submit');
            if(placeholder) $(this).find('input[name="EMAIL"]').attr('placeholder',placeholder);
            if(submit) $(this).find('input[type="submit"]').val(submit);
        })
        //Animate
		if($('.wow').length>0){
			new WOW().init();
		}
		$('.brand-item').each(function(){
			$(this).find('img').addClass('wobble-horizontal');
		})
		//Product Gallery
		if($('.product-gallery .bxslider').length>0){
			$('.product-gallery .bxslider').bxSlider({
				pagerCustom: '.product-gallery #bx-pager',
				nextText:'<i class="fa fa-angle-right" aria-hidden="true"></i>',
				prevText:'<i class="fa fa-angle-left" aria-hidden="true"></i>'
			});
		}
		$('.relate-product').each(function(){
			$(this).find('.nav-tabs li').first().addClass('active');
			$(this).find('.tab-content tab-pane').first().addClass('active');
		})
		//Count item cart
        if($("#count-cart-item").length){
            var count_cart_item = $("#count-cart-item").val();
            $(".cart-item-count").html(count_cart_item);
        }
        // add review click
        $('.add-review').on('click',function(event){
        	event.preventDefault();
        	if($("#sv-reviews").length > 0){
	        	$('a[href="#sv-reviews"]').trigger('click');
	        	$('html, body').animate({scrollTop:$("#review_form").offset().top-50}, 'slow');
	        }
        })
        //Back To Top
		$('.scroll-top').on('click',function(event){
			event.preventDefault();
			$('html, body').animate({scrollTop:0}, 'slow');
		});
		//Attr Filter Price		
	    $('.slider-range-price').each(function(){
	    	var seff = $(this);
	    	var price_max = Number(seff.attr('data-pmax'));
			var price_min = Number(seff.attr('data-pmin'));
			var currency = seff.attr('data-currency');
			seff.slider({
		      	range: true,
		      	min: price_min,
		      	max: price_max,
		      	values: [ price_min, price_max ],
		      	stop: function( event, ui ){
		      		seff.parent().find('.get-filter-price').trigger('click');
		        },
		      	slide: function( event, ui ) {
		        	seff.parent().find( ".amount-price" ).val( currency + ui.values[ 0 ] + " - "+currency + ui.values[ 1 ] );
		        	seff.parent().find( ".get-filter-price" ).val( ui.values[ 0 ] + "," + ui.values[ 1 ] );	        	
		      	}
		    });
		    seff.parent().find( ".amount-price" ).val( currency + seff.slider( "values", 0 ) +
		      " - "+currency + seff.slider( "values", 1 ) );
	    })
	    $('.attr-list li a').on('click',function(e){
	    	e.preventDefault();
	    	if($(this).hasClass('selected')) $(this).removeClass('selected');
	    	else $(this).addClass('selected');
	    })
	    
		//Product Masonry 
		if($('.list-product4').length>0){
            $('.list-product4 > div').imagesLoaded( function(){
                $('.list-product4 > div').masonry({
                    // options
                    itemSelector: '.item-product-masonry',
                });
            });
        }
        $('.footer-bg-data').each(function(){
        	var data_class = $(this).attr('data-bg');
        	$(this).parents('#footer').addClass(data_class);
        })
        $('.btn-reset-form').on('click',function(event){
        	event.preventDefault();
        	$(this).parents('.comment-form').find('.row input,.row textarea').val('');
        })
        //Top Deal
		$('.close-top-deal').on('click',function(event){
			event.preventDefault();
			$(this).parents('.top-banner-prl').slideUp();
			$("#header").css('min-height','');
		});
		if($('.deal-palallax').length>0){
			$(".deal-palallax").TimeCircles({
				fg_width: 0.05,
				bg_width: 0,
				text_size: 0,
				circle_bg_color: "transparent",
				time: {
					Days: {
						show: true,
						text: "days",
						color: "#fff"
					},
					Hours: {
						show: true,
						text: "hours",
						color: "#fff"
					},
					Minutes: {
						show: true,
						text: "minutes",
						color: "#fff"
					},
					Seconds: {
						show: true,
						text: "seconds",
						color: "#fff"
					}
				}
			}); 
		}
		if($('.countdown-master').length>0){
			$('.countdown-master').each(function(){
				var seconds = Number($(this).attr('data-time'));
				$(this).FlipClock(seconds,{
			        clockFace: 'HourlyCounter',
			        countdown: true,
			        autoStart: true,
			    });
			});
		}
		//Deal Gallery
		if($('.item-deal11').length>0){
			$('.deal-control > ul > li > a').on('click',function(event){
				event.preventDefault();
				$(this).parents('.item-deal11').find('.deal-control > ul > li > a').removeClass('active');
				$(this).addClass('active');
				var img = $(this).find('img');
				var thumb = $(this).parents('.item-deal11');
				thumb.find('.product-thumb-link img').attr('src',img.attr('src'));
				if(img.attr('srcset')) thumb.find('.product-thumb-link img').attr('srcset',img.attr('srcset'));
				else thumb.find('.product-thumb-link img').attr('srcset','');
				thumb.find('.product-thumb-link img').attr('alt',img.attr('alt'));
			});
		};
		//Tab Active
		$('.title-tab1').each(function(){
			$(this).append('<div class="ef-lamp"></div>');
			$(this).find('.ef-lamp').width($(this).find('li.active').width());
			var osl=$(this).find('li.active').offset().left-$(this).offset().left-1;
			$(this).find('.ef-lamp').css('left',osl+'px');
			$(this).find('.list-none li a').on('click',function(){
				$(this).parents('.title-tab1').find('.ef-lamp').width($(this).parent().width());
				var osl=$(this).offset().left-$(this).parents('.title-tab1').offset().left-1;
				$(this).parents('.title-tab1').find('.ef-lamp').css('left',osl+'px');
			});
		});
		//Shop The Look
		$('.box-hover-dir').each( function() {
			$(this).hoverdir(); 
		});
	});

	$(window).load(function(){
		s7upf_owl_slider();
		s7upf_all_slider();	
		set_pos_megamenu();	
		//Slick Slider
		if($('.banner-slider .slick').length>0){
			$('.banner-slider .slick').each(function(){
				$(this).slick({
					centerMode: true,
					infinite: true,
					centerPadding: '200px',
					slidesToShow: 1,
					prevArrow:'<div class="slick-prev"><div class="slick-prev-img"></div><div class="slick-nav"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></div></div>',
					nextArrow:'<div class="slick-next"><div class="slick-next-img"></div><div class="slick-nav"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></div></div>',
					responsive: [
					{
					  breakpoint: 1200,
					  settings: {
						centerPadding: '0px',
					  }
					}]
				});
				slick_control();
				$('.slick').on('afterChange', function(event){
					slick_control();
					slick_animated(event);
				});
			});
		}
		// menu fixed onload
		$("#header").css('min-height','');
        if($(window).width()>1024){
            $("#header").css('min-height',$("#header").height());
            fixed_header();
        }
        else{
            $("#header").css('min-height','');
        }
        // rtl-enable
        if($('.rtl-enable').length > 0){
            $('.vc_row[data-vc-full-width="true"]').each(function(){
                var style = $(this).attr('style');
                style = style.replace("left","right");
                $(this).attr('style',style);
            })
        }
        if($('.banner-list-cat').length>0){
        	if($('.rtl-enable').length > 0) $('.banner-list-cat').css('right',$('.banner-list-cat').parents('.container').offset().left+15);
        	else $('.banner-list-cat').css('left',$('.banner-list-cat').parents('.container').offset().left+15);
        }
		if($('.hotdeal-countdown').length>0){
			$(".hotdeal-countdown").TimeCircles({
				fg_width: 0.03,
				bg_width: 0,
				text_size: 0,
				circle_bg_color: "#000000",
				time: {
					Days: {
						show: true,
						text: "D",
						color: "#ffcc00"
					},
					Hours: {
						show: true,
						text: "H",
						color: "#ffcc00"
					},
					Minutes: {
						show: true,
						text: "M",
						color: "#ffcc00"
					},
					Seconds: {
						show: true,
						text: "S",
						color: "#ffcc00"
					}
				}
			}); 
		}
		$('.arrow-style14').each(function(){
			var left = $(this).parents('.contain-slider14').find('.title-box1 span').width();
			if(!left) left = 0;
			if($('.rtl-enable').length > 0) $(this).parents('.contain-slider14').find('.owl-controls').css('right',left+45);
			else $(this).parents('.contain-slider14').find('.owl-controls').css('left',left+45);
		})
		if($(window).width() < 768){
			if($('.main-nav').offset().left < $(window).width()/2) $('.main-nav>ul').css({'right':'auto','left':'0'});
			else $('.main-nav>ul').css({'right':'0','left':'auto'});
		}
		//menu fix
		if($(window).width() >= 768){
			var c_width = $(window).width();
			$('.main-nav ul ul ul.sub-menu').each(function(){
				var left = $(this).offset().left;
				if(c_width - left < 180){
					$(this).css({"left": "-100%"})
				}
				if(left < 180){
					$(this).css({"left": "100%"})
				}
			})
		}
		if($('.info-list-cart').length > 0){
            if($('.info-list-cart').height() >= 245) $('.info-list-cart').mCustomScrollbar();
        }
	});// End load

	/* ---------------------------------------------
     Scripts resize
     --------------------------------------------- */
    var w_width = $(window).width();
    $(window).resize(function(){
    	if($('.banner-list-cat').length>0){
        	if($('.rtl-enable').length > 0) $('.banner-list-cat').css('right',$('.banner-list-cat').parents('.container').offset().left+15);
        	else $('.banner-list-cat').css('left',$('.banner-list-cat').parents('.container').offset().left+15);
        }
        $('.main-nav>ul').attr('style','');
        if($(window).width() < 768){
			if($('.main-nav').offset().left < $(window).width()/2) $('.main-nav>ul').css({'right':'auto','left':'0'});
			else $('.main-nav>ul').css({'right':'0','left':'auto'});
		}
        $("#header").css('min-height','');
        var c_width = $(window).width();
        setTimeout(function() {
            if($('.rtl-enable').length > 0 && c_width != w_width){
                $('.vc_row[data-vc-full-width="true"]').each(function(){
                    var style = $(this).attr('style');
                    style = style.replace(" left:"," right:");
                    $(this).attr('style',style);
                })
                w_width = c_width;
            }
        }, 3000);
    });

	jQuery(window).scroll(function(){
		fixed_header();
		if($(window).width()>1024){
            $("#header").css('min-height',$("#header").height());
            fixed_header();
        }
        else{
            $("#header").css('min-height','');
        }
		// set_pos_megamenu();
		//Scroll Top
		if($(this).scrollTop()>$(this).height()){
			$('.scroll-top').addClass('active');
		}else{
			$('.scroll-top').removeClass('active');
		}
	});// End Scroll

	$( document ).ajaxComplete(function( event,request, settings ) {
		fix_compare_link();
	});

})(jQuery);