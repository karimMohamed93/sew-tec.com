<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package 7up-framework
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(). '/karim.css'?>"></link>
<script src="<?php echo get_template_directory_uri(). '/script.js'?>"></script>
<?php wp_head(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/assets/css/mystyle.css">
</head>
<body <?php body_class(); ?>>
<div class="wrap">
    <?php
    $page_id = s7upf_get_value_by_id('s7upf_header_page');
    if(!empty($page_id)){
       s7upf_get_header_visual($page_id);
    }
    else{
        s7upf_get_header_default();
    }?>

